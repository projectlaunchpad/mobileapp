/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React from 'react';
import { Image, Text, StyleProp, ViewStyle, TouchableOpacity, Dimensions, GestureResponderEvent } from 'react-native';

import { normalize } from '../services/globals';

import TitleSmallBG from '../assets/ui/title-small.png';

type TitleButtonProps = {
    title: string;
    style?: StyleProp<ViewStyle>;
    disabled?: boolean;
    onPress: ((event: GestureResponderEvent) => void) | undefined;
};

export default function TitleButton({ title, onPress, disabled = false, style }: TitleButtonProps) {
    return (
        <TouchableOpacity
            onPress={onPress}
            disabled={disabled}
            style={[
                style,
                {
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: Dimensions.get('window').width * 0.5,
                },
            ]}
        >
            <Image
                source={TitleSmallBG}
                style={{
                    width: Dimensions.get('window').width * 0.5,
                    height: '100%',
                    position: 'absolute',
                    top: 0,
                    opacity: disabled?0.3:1,
                }}
                resizeMode="stretch"
            />
            <Text
                style={{
                    color: 'white',
                    fontFamily: 'TextFont',
                    fontSize: normalize(14),
                    opacity: disabled ? 0.3 : 1,
                }}
            >
                {title}
            </Text>
        </TouchableOpacity>
    );
}
