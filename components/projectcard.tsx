/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React, { useEffect, useRef, useState } from 'react';
import { Image, Text, StyleProp, ViewStyle, TouchableOpacity, GestureResponderEvent, ImageSourcePropType, View, TextStyle } from 'react-native';
import CardFlip from 'react-native-card-flip';
import { useIsFocused } from '@react-navigation/native';

import { normalize } from '../services/globals';

import FrontImage from '../assets/cards/front-project.png';
import BackImage from '../assets/cards/back-project.png';
import IconTwoCards from '../assets/ui/icon-cards-2.png';
import IconThreeCards from '../assets/ui/icon-cards-3.png';
import IconFourCards from '../assets/ui/icon-cards-4.png';
import { getColorBlockByColorId } from '../services/imageassets';

type ProjectCardProps = {
    title: string;
    artwork: ImageSourcePropType;
    effectIcons?: string[];
    effectArgs?: string[];
    colors: string[];
    payout: Record<number, number>;
    initialHidden: boolean;
    autoReveal: boolean;
    style?: StyleProp<ViewStyle>;
    titleStyle?: StyleProp<TextStyle>;
    sellStyle?: StyleProp<TextStyle>;
    onPressIcon?: ((event: GestureResponderEvent, effectIcon: string) => void) | undefined;
    onPress?: ((event: GestureResponderEvent) => void) | undefined;
};

export default function ProjectCard({ title, artwork, effectIcons = [], effectArgs = [], colors, payout, initialHidden, autoReveal, onPress, onPressIcon, style, titleStyle, sellStyle }: ProjectCardProps) {
    const cardRef = useRef<CardFlip>(null);
    const [isHidden, setIsHidden] = useState<boolean>(initialHidden);

    const isFocused = useIsFocused();
    useEffect(() => {
        const revealCard = () => {
            setTimeout(() => {
                doFlip();
            }, 500)
        };
        if (autoReveal)
            revealCard();
    }, [isFocused]);

    const doFlip = () => {
        if (isHidden) {
            cardRef.current!.flip();
            setIsHidden(!isHidden);
        }
    }

    const renderFront = () => {
        return (
            <TouchableOpacity onPress={onPress} activeOpacity={1} style={{ width: '100%', height: '100%' }} >
                <Image source={FrontImage} style={{ width: '100%', height: '100%', position: 'absolute' }} resizeMode='stretch' />
                <Image source={artwork} style={{ width: '90%', height: '100%', left: '5%', position: 'absolute' }} resizeMode='contain' />
                <View style={{ width: '100%', height: '100%', alignItems: 'flex-start', justifyContent: 'flex-start', position: 'absolute' }}>
                    <Text numberOfLines={1} style={[{ fontFamily: 'HeaderFont', color: '#324764' }, titleStyle]}>{title.toUpperCase()}</Text>
                </View>
                <View style={{ width: '90%', height: '15%', bottom: 0, left: '4%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', position: 'absolute' }}>
                    <View style={{ width: '33%', height: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Image source={IconTwoCards} style={{ width: '50%', height: '100%' }} resizeMode='contain' />
                        <Text style={[{ fontFamily: 'HeaderFont', color: '#324764', alignSelf: 'center' }, sellStyle]}>{payout[2]}</Text>
                    </View>
                    <View style={{ width: '33%', height: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Image source={IconThreeCards} style={{ width: '50%', height: '100%' }} resizeMode='contain' />
                        <Text style={[{ fontFamily: 'HeaderFont', color: '#324764', alignSelf: 'center' }, sellStyle]}>{payout[3]}</Text>
                    </View>
                    <View style={{ width: '33%', height: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Image source={IconFourCards} style={{ width: '50%', height: '100%' }} resizeMode='contain' />
                        <Text style={[{ fontFamily: 'HeaderFont', color: '#324764', alignSelf: 'center', marginLeft: '2%' }, sellStyle]}>{payout[4]}</Text>
                    </View>
                </View>
                <View style={{ width: '10%', height: '70%', right: 0, top: '15%', flexDirection: 'column', alignItems: 'center', justifyContent: 'space-between', position: 'absolute' }}>
                    <View style={{ width: '100%', height: '30%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Image source={getColorBlockByColorId(colors[0])} style={{ width: '100%', height: '100%' }} resizeMode='stretch' />
                    </View>
                    <View style={{ width: '100%', height: '30%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Image source={getColorBlockByColorId(colors[1])} style={{ width: '100%', height: '100%' }} resizeMode='stretch' />
                    </View>
                    <View style={{ width: '100%', height: '30%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }}>
                        <Image source={getColorBlockByColorId(colors[2])} style={{ width: '100%', height: '100%' }} resizeMode='stretch' />
                    </View>
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <View style={style}>
            {initialHidden &&
                <CardFlip style={style} ref={cardRef}>
                    <TouchableOpacity activeOpacity={1} style={{ width: '100%', height: '100%' }} onPress={() => doFlip()} >
                        <Image source={BackImage} style={{ width: '100%', height: '100%' }} resizeMode='contain' />
                    </TouchableOpacity>
                    {renderFront()}
                </CardFlip>
            }
            {!initialHidden && renderFront()}
            {initialHidden && !autoReveal &&
                <View style={{ alignItems: 'center', justifyContent: 'flex-start' }}>
                    <Text style={{ fontFamily: 'TextFont', color: 'white' }}>Tap to reveal!</Text>
                </View>
            }
        </View>
    );
}
