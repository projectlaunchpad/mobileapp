/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React, { useEffect, useRef, useState } from 'react';
import { Image, Text, StyleProp, ViewStyle, TouchableOpacity, GestureResponderEvent, ImageSourcePropType, View, TextStyle } from 'react-native';
import CardFlip from 'react-native-card-flip';
import { useIsFocused } from '@react-navigation/core';

import { getColorBlockByColorId, getEffectIconById, getResourceFrontByColorId } from '../services/imageassets';

import BackImage from '../assets/cards/back-resource.png';
import IconCredits from '../assets/ui/icon-credits.png';
import Effect from './effect';

type ResourceCardProps = {
    title: string;
    artwork: ImageSourcePropType;
    cost: number;
    effectIcons?: string[];
    effectArgs?: string[];
    color: string;
    initialHidden: boolean;
    autoReveal: boolean;
    style?: StyleProp<ViewStyle>;
    titleStyle?: StyleProp<TextStyle>;
    effectStyle?: StyleProp<TextStyle>;
    onPressEffect?: ((effectIcon: string) => void) | undefined;
    onPress?: ((event: GestureResponderEvent) => void) | undefined;
};

export default function ResourceCard({ title, artwork, cost, effectIcons = [], effectArgs = [], color, initialHidden, autoReveal, onPress, onPressEffect, style, titleStyle, effectStyle }: ResourceCardProps) {

    const cardRef = useRef<CardFlip>(null);
    const [isHidden, setIsHidden] = useState<boolean>(initialHidden);

    const isFocused = useIsFocused();
    useEffect(() => {
        const revealCard = () => {
            setTimeout(() => {
                doFlip();
            }, 500)
        };
        if (autoReveal)
            revealCard();
    }, [isFocused]);

    const doFlip = () => {
        if (isHidden) {
            cardRef.current!.flip();
            setIsHidden(!isHidden);
        }
    }

    const frontImage: ImageSourcePropType = getResourceFrontByColorId(color);

    const onPressEffectHandler = (effectIcon: string) => {
        if (onPressEffect)
            onPressEffect(effectIcon);
    }

    const renderFront = (): JSX.Element => {
        return (
            <TouchableOpacity onPress={onPress} activeOpacity={1} style={{ width: '100%', height: '100%' }} >
                <Image source={frontImage} style={{ width: '100%', height: '100%', position: 'absolute' }} resizeMode='stretch' />
                <Image source={artwork} style={{ width: '90%', height: '100%', left: '5%', position: 'absolute' }} resizeMode='contain' />
                <View style={{ width: '100%', height: '100%', alignItems: 'flex-start', justifyContent: 'flex-start' }}>
                    <Text numberOfLines={1} style={ [ { fontFamily: 'HeaderFont', color: '#324764' }, titleStyle ] }>{title.toUpperCase()}</Text>
                </View>
                <View style={{ width: '33%', height: '7%', right: 0, top: '7%', alignItems: 'flex-end', justifyContent: 'flex-end', position: 'absolute', flexDirection: 'row', marginRight: '5%' }}>
                    <Image source={IconCredits} style={{ width: '33%', height: '100%', marginLeft: '11%' }} resizeMode='contain' />
                    { cost > 1 && <Image source={IconCredits} style={{ width: '33%', height: '100%', marginLeft: '-11%' }} resizeMode='contain' /> }
                    { cost > 2 && <Image source={IconCredits} style={{ width: '33%', height: '100%', marginLeft: '-11%' }} resizeMode='contain' /> }
                </View>
                <View style={{ width: '100%', height: '14%', bottom: '5%', alignItems: 'center', justifyContent: 'center', position: 'absolute', flexDirection: 'row' }}>
                    <Effect onPress={(effectIcon: string) => onPressEffectHandler(effectIcon)} effectIcons={effectIcons} effectArgs={effectArgs} style={{ width: '100%', height: '100%' }} labelStyle={effectStyle} />
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <View style={style}>
            {initialHidden &&
                <CardFlip style={style} ref={cardRef}>
                    <TouchableOpacity activeOpacity={1} style={{ width: '100%', height: '100%' }} onPress={() => doFlip() } >
                        <Image source={BackImage} style={{ width: '100%', height: '100%' }} resizeMode='contain' />
                    </TouchableOpacity>
                    { renderFront() }
                </CardFlip>
            }
            {!initialHidden && renderFront() }
            {initialHidden && !autoReveal &&
                <View style={{ alignItems: 'center', justifyContent: 'flex-start' }}>
                    <Text style={{ fontFamily: 'TextFont', color: 'white' }}>Tap to reveal!</Text>
                </View>
            }
        </View>
    );
}
