/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React from 'react';

import { Image, Text, StyleProp, ViewStyle, View, Dimensions } from 'react-native';

import { normalize } from '../services/globals';
import TitleLargeBG from '../assets/ui/title-large.png';

type TitleLargeProps = {
    title: string;
    style?: StyleProp<ViewStyle>;
};

export default function TitleLarge({ title, style }: TitleLargeProps) {
    return (
        <View
            style={[
                style,
                {
                    alignItems: 'center',
                    justifyContent: 'center',
                    width: Dimensions.get('window').width * 0.95,
                    paddingHorizontal: 10
                },
            ]}
        >
            <Image
                source={TitleLargeBG}
                style={{
                    width: Dimensions.get('window').width * 0.95,
                    height: '100%',
                    position: 'absolute',
                    top: 0,
                }}
                resizeMode="stretch"
            />
            <Text
                numberOfLines={1}
                style={{
                    color: 'white',
                    fontFamily: 'HeaderFont',
                    fontSize: normalize(24),
                }}
            >
                {title}
            </Text>
        </View>
    );
}
