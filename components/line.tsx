/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for select.
 */


import React from 'react';
import { Image, View, StyleProp, ViewStyle } from 'react-native';

import LineBG from '../assets/ui/line.png';

type LineProps = {
    size: string;
    style?: StyleProp<ViewStyle>;
};

export default function Line({ size, style }: LineProps) {
    return (
        <View
            style={[
                style,
                {
                    width: '100%',
                    alignItems: 'center',
                    justifyContent: 'center',
                },
            ]}
        >
            <Image source={ LineBG } resizeMode="stretch" style={{ width: size == 'small' ? '50%' : '70%' }} />
        </View>
    );
}
