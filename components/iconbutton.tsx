/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React from 'react';
import { Image, TouchableOpacity, GestureResponderEvent, ImageSourcePropType } from 'react-native';
import { MaterialCommunityIcons } from '@expo/vector-icons';

type IconButtonProps = {
    image: ImageSourcePropType;
    onPress: ((event: GestureResponderEvent) => void) | undefined;
    disabled: boolean;
    alert: boolean;
    width: number;
    height: number;
    margin: number;
};

export default function IconButton({ image, onPress, disabled, alert, width, height, margin }: IconButtonProps) {
    return (
        <TouchableOpacity disabled={disabled} onPress={onPress} style={{ margin: margin, width: width, height: height }}>
            <Image
                source={image}
                resizeMode="cover"
                style={{
                    opacity: disabled ? 0.5 : 1,
                    width: width,
                    height: height,
                }}
            />
            {alert && (
                <MaterialCommunityIcons
                    style={{
                        position: 'absolute',
                        right: -5,
                        top: -5,
                        color: 'orange',
                        zIndex: 1,
                    }}
                    name="alert-decagram"
                    size={24}
                />
            )}
        </TouchableOpacity>
    );
}
