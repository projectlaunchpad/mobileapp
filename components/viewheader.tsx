/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React from 'react';
import { Text, View, GestureResponderEvent } from 'react-native';

import { normalize } from '../services/globals';
import IconButton from './iconbutton';

import IconClose from '../assets/ui/icon-close.png';

type ViewHeaderProps = {
    title: string;
    onPressBack: ((event: GestureResponderEvent) => void) | undefined;
};

export default function ViewHeader({ title, onPressBack }: ViewHeaderProps) {
    return (
        <View
            style={{
                position: 'absolute',
                flexDirection: 'row',
                height: 80,
                paddingTop: 30,
                alignItems: 'center',
                width: '100%',
            }}
        >
            <View style={{ width: 20 }} />
            <View style={{ flex: 1 }}>
                <Text
                    numberOfLines={1}
                    style={{
                        fontFamily: 'HeaderFont',
                        fontSize: normalize(24),
                        color: '#324764',
                        textAlign: 'left',
                    }}
                >
                    {title}
                </Text>
            </View>
            <View style={{ alignContent: 'center', justifyContent: 'center' }}>
                <IconButton image={IconClose} disabled={false} alert={false} onPress={onPressBack} width={30} height={30} margin={15} />
            </View>
        </View>
    );
}
