/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React from 'react';
import { Image, Text, View, StyleProp, TextStyle, ViewStyle, GestureResponderEvent } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';

import { getColorBlockByColorId, getEffectIconById } from '../services/imageassets';

type EffectProps = {
    effectIcons: string[];
    effectArgs: string[];
    style?: StyleProp<ViewStyle>;
    labelStyle?: StyleProp<TextStyle>;
    onPress: ((effectIcon: string) => void);
};

export default function Effect({ effectIcons, effectArgs, onPress, style, labelStyle }: EffectProps) {
    
    const renderEffectArgs = (effectId: string, effectArgs: string[]): JSX.Element => {
        switch (effectId) {
            case 'add_take_pool':
                return (
                    <></>
                );
            case 'sell_bonus':
                return (
                    <Text style={[labelStyle, { color: '#324764' }]}>{effectArgs[0]}</Text>
                );
            case 'add_take_resources':
                return (
                    <></>
                );
            case 'add_play_card':
                return (
                    <></>
                );
            case 'sell_bonus_color':
                return (
                    <View style={{ flexDirection: 'row', height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={[labelStyle, { color: '#324764' } ]}>{effectArgs[0]}</Text>
                        {effectArgs.length > 1 && <Image source={getColorBlockByColorId(effectArgs[1])} resizeMode='stretch' style={{ width: '30%', height: '70%', marginLeft: 4 }} />}
                        {effectArgs.length > 2 && <Image source={getColorBlockByColorId(effectArgs[2])} resizeMode='stretch' style={{ width: '30%', height: '70%', marginLeft: 4 }} />}
                    </View>
                );
            case 'credit_bonus':
                return (
                    <View style={{ flexDirection: 'row', height: '100%', alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={[labelStyle, { color: '#324764' }]}>{effectArgs[0]}</Text>
                        <Image source={getEffectIconById(effectArgs[1])} style={{ height: '100%', width: '20%' }} resizeMode='contain' />
                    </View>
                );
            case 'add_draw_discard_pool':
                return (
                    <></>
                );
            case 'add_getpool_discard_pool':
                return (
                    <></>
                );
            default:
                return (
                    <View>
                        {
                            effectArgs.map((effectArg) => {
                                return (
                                    <Text key={effectArg} style={[labelStyle, { color: '#324764' }]}>{effectArg}</Text>
                                )
                            })
                        }
                    </View>
                );
        }
    }
    
    return (
        <TouchableOpacity onPress={() => onPress(effectIcons[0])} style={[style, { flexDirection: 'row', alignItems: 'center', justifyContent: 'center' }]}>
            {effectIcons.map((effectID) => {
                return (
                    <Image key={effectID} source={getEffectIconById(effectID)} style={{ height: '100%', width: '20%' }} resizeMode='contain' />
                )
            })}
            {renderEffectArgs(effectIcons[0], effectArgs)}
        </TouchableOpacity>
    );
}
