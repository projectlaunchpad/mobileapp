/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React, { useEffect, useRef, useState } from 'react';
import { Image, Text, StyleProp, ViewStyle, TouchableOpacity, GestureResponderEvent, ImageSourcePropType, View, TextStyle } from 'react-native';
import CardFlip from 'react-native-card-flip';
import { useIsFocused } from '@react-navigation/native';

import { normalize } from '../services/globals';

import BackImage from '../assets/cards/back-persona.png';
import Effect from './effect';

type PersonaCardProps = {
    title: string;
    artwork: ImageSourcePropType;
    effectIcons?: string[];
    effectArgs?: string[];
    initialHidden: boolean;
    autoReveal: boolean;
    style?: StyleProp<ViewStyle>;
    titleStyle?: StyleProp<TextStyle>;
    effectStyle?: StyleProp<TextStyle>;
    onPressEffect?: ((effectIcon: string) => void) | undefined;
    onPress?: ((event: GestureResponderEvent) => void) | undefined;
};

export default function PersonaCard({ title, artwork, effectIcons = [], effectArgs = [], initialHidden, autoReveal, onPress, onPressEffect, style, titleStyle, effectStyle }: PersonaCardProps) {

    const cardRef = useRef<CardFlip>(null);
    const [isHidden, setIsHidden] = useState<boolean>(initialHidden);

    const isFocused = useIsFocused();
    useEffect(() => {
        const revealCard = () => {
            setTimeout(() => {
                doFlip();
            }, 500)
        };
        if (autoReveal)
            revealCard();
    }, [isFocused]);

    const doFlip = () => {
        if (isHidden) {
            cardRef.current!.flip();
            setIsHidden(!isHidden);
        }
    }

    const onPressEffectHandler = (effectIcon: string) => {
        if (onPressEffect)
            onPressEffect(effectIcon);
    }

    const renderFront = () => {
        return (
            <TouchableOpacity onPress={onPress} activeOpacity={1} style={{ width: '100%', height: '100%' }} >
                <Image source={artwork} style={{ width: '100%', height: '100%', position: 'absolute' }} resizeMode='stretch' />
                <View style={{ width: '100%', height: '100%', alignItems: 'center', justifyContent: 'flex-start' }}>
                    <Text numberOfLines={1} style={[{ fontFamily: 'HeaderFont', color: '#324764' }, titleStyle]}>{title.toUpperCase()}</Text>
                </View>
                <View style={{ width: '100%', height: '15%', bottom: '4%', alignItems: 'center', justifyContent: 'center', position: 'absolute', flexDirection: 'row' }}>
                    <Effect onPress={(effectIcon: string) => onPressEffectHandler(effectIcon)} effectIcons={effectIcons} effectArgs={effectArgs} style={{ width: '100%', height: '100%' }} labelStyle={effectStyle} />
                </View>
            </TouchableOpacity>
        )
    }

    return (
        <View style={style}>
            {initialHidden &&
                <CardFlip style={style} ref={cardRef}>
                    <TouchableOpacity activeOpacity={1} style={{ width: '100%', height: '100%' }} onPress={() => doFlip()} >
                        <Image source={BackImage} style={{ width: '100%', height: '100%' }} resizeMode='contain' />
                    </TouchableOpacity>
                    {renderFront()}
                </CardFlip>
            }
            {!initialHidden && renderFront()}
            {initialHidden && !autoReveal &&
                <View style={{ alignItems: 'center', justifyContent: 'flex-start' }}>
                    <Text style={{ fontFamily: 'TextFont', color: 'white' }}>Tap to reveal!</Text>
                </View>
            }
        </View>
    );
}
