/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React from 'react';
import { Image, TouchableOpacity, GestureResponderEvent, View, StyleProp, ViewStyle, ImageSourcePropType, Text } from 'react-native';

import { CARD_TYPE_EVENT, CARD_TYPE_PERSONA, CARD_TYPE_PROJECT, CARD_TYPE_RESOURCE, normalize } from '../services/globals';
import { getImageAssetById } from '../services/imageassets';
import { Card } from '../model/card';
import EventCard from './eventcard';
import ResourceCard from './resourcecard';

import BackImageResource from '../assets/cards/back-resource.png';
import BackImageProject from '../assets/cards/back-project.png';
import BackImageEvent from '../assets/cards/back-event.png';
import BackImagePersona from '../assets/cards/back-persona.png';

type CardStackProps = {
    type: string;
    name: string;
    fontSize: number;
    cardFontSize: number;
    cardsLeft: number;
    openCard?: Card | undefined;
    onPress?: ((event: GestureResponderEvent) => void) | undefined;
    style?: StyleProp<ViewStyle>;
};

export default function CardStack({ type, name, fontSize, cardFontSize, cardsLeft, openCard, onPress, style }: CardStackProps) {

    let imageRes: ImageSourcePropType;
    switch (type) {
        case CARD_TYPE_RESOURCE:
            imageRes = BackImageResource;
            break;
        case CARD_TYPE_PROJECT:
            imageRes = BackImageProject;
            break;
        case CARD_TYPE_EVENT:
            imageRes = BackImageEvent;
            break;
        case CARD_TYPE_PERSONA:
            imageRes = BackImagePersona;
            break;
        default:
            imageRes = BackImageResource;
    }
    
    return (
        <View style={style}>
            <TouchableOpacity activeOpacity={1} style={{ width: '100%', height: '100%', flex: 1, flexDirection: 'column' }} onPress={ onPress } >
                {!openCard && <Image source={imageRes} style={{ width: '100%', height: '100%' }} resizeMode='contain' /> }
                {openCard && type == CARD_TYPE_EVENT && <EventCard onPress={onPress} title={openCard.name[0]} effectIcons={[openCard.effectid]} effectArgs={openCard.effectargs} artwork={getImageAssetById(openCard.illustrationid)} initialHidden={false} autoReveal={false} titleStyle={{ fontSize: normalize(cardFontSize), paddingTop: 1, paddingLeft: 3 }} effectStyle={{ fontSize: normalize(cardFontSize) }}/>}
                {openCard && type == CARD_TYPE_RESOURCE && <ResourceCard onPress={onPress} title={openCard.name[0]} effectIcons={[openCard.effectid]} effectArgs={openCard.effectargs} artwork={getImageAssetById(openCard.illustrationid)} initialHidden={false} autoReveal={false} cost={openCard.cost} color={openCard.colors[0]} titleStyle={{ fontSize: normalize(cardFontSize), paddingTop: 1.5, paddingLeft: 3 }} effectStyle={{ fontSize: normalize(cardFontSize) }}/>}
                <Text style={{ color: '#324764', fontFamily: 'TextFont', fontSize: normalize(fontSize), marginTop: 3, alignSelf: 'center' }}>{name}</Text>
                <Text style={{ color: '#324764', fontFamily: 'TextFont', fontSize: normalize(fontSize), alignSelf: 'center' }}>{cardsLeft} left</Text>
                <Image source={imageRes} style={{ width: '100%', height: '100%', position: 'absolute', marginLeft: -2, marginTop: -2, zIndex: -10 }} resizeMode='contain' />
                <Image source={imageRes} style={{ width: '100%', height: '100%', position: 'absolute', marginLeft: -4, marginTop: -4, zIndex: -10 }} resizeMode='contain' />
            </TouchableOpacity>
        </View>
    );
}
