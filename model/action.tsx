/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

export interface Action {
	ID: string
	name: string
	type: string
	timestamp: number
	sourceid: string
	targetid: string
	objectids: string[]
}
