/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

export interface Stack {
	ID: string | undefined;
	type: string | undefined;
	size: number | undefined;
	cardids: string[] | undefined
}
