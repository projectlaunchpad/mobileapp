/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

export interface Card {
	ID: string
	type: string
	name: string[]
	description: string[]
	illustrationid: string
	iconid: string
	effectid: string
	effectargs: string[]
	colors: string[]
	cost: number
	payout: Record<number, number>
}
