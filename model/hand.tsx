/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

export interface Hand {
	ID: string
	size: number
	cardids: string[]
}
