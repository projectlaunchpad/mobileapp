/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */
export interface Player {
	ID: string
	gameid: string
	sessionToken: string
	name: string
	personacardid: string
	handid: string
	isfirstplayer: boolean
	credits: number
	projectids: string[]
	cardchoicebufferids: string[]
}