/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

export interface Project {
	ID: string
	cardid: string
	playerid: string
	resourcecardids: string[]
	colors: string[]
	payout: Record<number, number>;
	playeffectid: string
	effectargs: string[]
}
