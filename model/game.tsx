/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

export interface Game {
	ID: string;
	status: string;
	invitecode: string;
	playerids: string[];
	poolstackid: string;
	resourcestackid: string;
	resourcediscardstackid: string;
	projectstackid: string;
	projectdiscardstackid: string;
	eventstackid: string;
	eventdiscardstackid: string;
	personastackid: string;
	personadiscardstackid: string;
	currenteventcardid: string;
	actionids: string[];
	currentplayerid: string;
	firstplayerid: string;
}
