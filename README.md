# Rush for the Web Mobile App

This is the Rush for the Web mobile app. It uses the Expo and react-native frameworks to provide mobile app implementations for iOS and Android phones. 

The app interacts with the backend service to provide online games and matchmaking.

## Running in Debug Mode

The app can be run in debug mode on a real Android device connected to the development machine using

```
yarn android
```

Refer to the [Expo documentation](https://expo.dev/) for more details on how to run and debug Expo apps.

The service URL for the backend is set in the `app.json` file with the `serviceURLDev` key.

## Building Releases

For release builds, the Expo service is used. Please refer to the [Expo documentation](https://expo.dev/) for details.

## Licenses

This application uses the Noto Font
Copyright (c) 2013, Google Inc.

This Font Software is licensed under the SIL Open Font License, Version 1.1.
This license is copied below, and is also available with a FAQ at:
http://scripts.sil.org/OFL