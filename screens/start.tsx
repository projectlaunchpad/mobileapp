/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React, { useEffect } from 'react';
import { Text, TextInput, View, Image, ImageBackground } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';

import { RootStackParamList } from '../App';
import { normalize } from '../services/globals';
import { Player } from '../model/player';
import TitleButton from '../components/titlebutton';
import Line from '../components/line';
import { createGame, fetchCards, getGame, joinGame } from '../services/middleware';
import { Log } from '../services/log';
import * as Updates from 'expo-updates';
import Constants from 'expo-constants';

import background from '../assets/ui/background-start.png';
import artwork from '../assets/ui/logo.png';

type StartScreenNavigationProp = StackNavigationProp<RootStackParamList, 'StartScreen'>;

type Props = {
    navigation: StartScreenNavigationProp;
};

export default function StartScreen({ navigation }: Props) {
    const [playername, onChangePlayername] = React.useState("");
    const [invitecode, onChangeInviteCode] = React.useState("");

    useEffect(() => {
        Log.info('initializing app, fetching card database..');
        fetchCards();
    }, []);

    const onPressStartButton = () => {
        createGame(playername).then((player: Player) => {
            Log.info('successfully created game for player ' + player.ID);
            getGame(player.gameid).then((game) => {
                Log.info('successfully created game ' + game.ID);
                navigation.navigate('LobbyScreen', {
                    inviteCode: game.invitecode,
                    localPlayerName: player.name,
                    localPlayerID: player.ID,
                    gameID: game.ID
                });
            }).catch((err) => {
                Log.error(err);
                openModal();
            });
        }).catch(err => {
            Log.error(err);
            openModal();
        });
    };

    const onPressJoinButton = () => {
        joinGame(invitecode, playername).then((player: Player) => {
            Log.info('successfully created game for player ' + player.ID);
            getGame(player.gameid).then((game) => {
                Log.info('successfully joined game ' + game.ID);
                navigation.navigate('LobbyScreen', {
                    inviteCode: game.invitecode,
                    localPlayerName: player.name,
                    localPlayerID: player.ID,
                    gameID: game.ID
                });
            }).catch((err) => {
                Log.error(err);
                openModal();
            });
        }).catch(err => {
            Log.error(err);
            openModal();
        });

    };

    const openModal = () => {
        navigation.navigate('Modal', {
            title: 'Error occured!',
            description: 'An error has occured, please try again.',
            button1Text: 'Ok',
            onButton1Press: () => navigation.goBack()
        });
    }

    return (
        <ImageBackground source={background} style={{ flex: 1 }}>
            <View
                style={{
                    flex: 1,
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Image source={artwork} style={{ width: 350, height: 350, marginTop: 100 }} />
                </View>
                <View style={{ flex: 1, width: '100%', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                    <TextInput
                        style={{
                            width: '80%',
                            borderColor: '#324764',
                            borderWidth: 1,
                            textAlign: 'center',
                            marginBottom: 10,
                            fontFamily: 'TextFont',
                            color: '#324764',
                            backgroundColor: '#d8d8d8dd'
                        }}
                        placeholder='Your Name'
                        placeholderTextColor='gray'
                        onChangeText={(text) => onChangePlayername(text)}
                        value={playername}
                    />
                    <TitleButton title='Start New Game' onPress={onPressStartButton} style={{ height: 40 }}/>
                    <Line size='large' style={{ marginTop: 20 }}/>
                    <Text style={{ fontFamily: 'HeaderFont', fontSize: normalize(14), color: '#324764', marginTop: 7 }}>or</Text>
                    <Line size='large' style={{ marginTop: 10 }}/>
                    <TextInput
                        style={{
                            width: '80%',
                            borderColor: '#324764',
                            borderWidth: 1,
                            textAlign: 'center',
                            marginTop: 20,
                            marginBottom: 10,
                            fontFamily: 'TextFont',
                            color: '#324764',
                            backgroundColor: '#d8d8d8dd'
                        }}
                        placeholder='Your Name'
                        placeholderTextColor='gray'
                        onChangeText={(text) => onChangePlayername(text)}
                        value={playername}
                    />
                    <TextInput
                        style={{
                            width: '80%',
                            borderColor: '#324764',
                            borderWidth: 1,
                            textAlign: 'center',
                            marginBottom: 10,
                            fontFamily: 'TextFont',
                            color: '#324764',
                            backgroundColor: '#d8d8d8dd'
                        }}
                        placeholder='Invitecode'
                        placeholderTextColor='gray'
                        autoCompleteType='password'
                        onChangeText={(text) => onChangeInviteCode(text)}
                        value={invitecode}
                    />
                    <TitleButton title='Join Game' onPress={onPressJoinButton} style={{ height: 40 }}/>
                </View>
                <Text style={{ color: 'darkgrey' }}>{Updates.releaseChannel} -- {Constants.manifest!.version}</Text>
            </View>
        </ImageBackground>
    );
}
