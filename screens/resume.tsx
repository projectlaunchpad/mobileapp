/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React, { useEffect } from 'react';
import { Text, View, Image, ImageBackground } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { useIsFocused } from '@react-navigation/native';

import { RootStackParamList } from '../App';
import { Game } from '../model/game';
import { normalize } from '../services/globals';
import TitleButton from '../components/titlebutton';
import Line from '../components/line';
import { getGame } from '../services/middleware';
import { Log } from '../services/log';

import background from '../assets/ui/background-screen.png';
import artwork from '../assets/ui/icon-missing.png';

type ResumeScreenNavigationProp = StackNavigationProp<RootStackParamList, 'ResumeScreen'>;

type Props = {
    navigation: ResumeScreenNavigationProp;
    gameID: string
    localPlayerID: string
};

export default function ResumeScreen({ navigation, gameID, localPlayerID }: Props) {
    const [game, setGame] = React.useState<Game>();

    const isFocused = useIsFocused();
    useEffect(() => {
        const updateGame = async () => {
            Log.info('updating state for game id ' + gameID);
            await getGame(gameID).then(async (game: Game) => {
                setGame(game);
            });
        };
        updateGame();
    }, [isFocused]);

    const openModal = () => {
        navigation.navigate('Modal', {
            title: 'Error occured!',
            description: 'An error has occured, please try again.',
            button1Text: 'Ok',
            onButton1Press: () => navigation.goBack()
        });
    }

    return (
        <ImageBackground source={background} style={{ flex: 1 }}>
            <View
                style={{
                    flex: 1,
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    <Image source={artwork} style={{ width: 100, height: 100 }} />
                </View>
                <View style={{ flex: 1, width: '100%', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                    <Line size='large' style={{ marginTop: 20 }}/>
                    <Text style={{ fontFamily: 'HeaderFont', fontSize: normalize(14), color: 'white', marginTop: 7 }}>Game Resume</Text>
                    <Line size='large' style={{ marginTop: 10 }}/>
                    <TitleButton title='Join Game' onPress={() => console.log('share')} style={{ height: 40 }}/>
                </View>
            </View>
        </ImageBackground>
    );
}
