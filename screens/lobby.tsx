/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React, { useEffect } from 'react';
import { Text, TextInput, View, Image, ImageBackground, ActivityIndicator, Share } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';

import { Game } from '../model/game';
import { Player } from '../model/player';
import TitleButton from '../components/titlebutton';
import Line from '../components/line';
import ViewHeader from '../components/viewheader';
import { RootStackParamList } from '../App';
import { GAME_STATE_RUNNING, normalize } from '../services/globals';
import { Log } from '../services/log';
import { getGame, getPlayer, startGame } from '../services/middleware';

import background from '../assets/ui/background-screen.png';
import artwork from '../assets/ui/logo.png';

type LobbyScreenNavigationProp = StackNavigationProp<RootStackParamList, 'LobbyScreen'>;
type LobbyScreenRouteProp = RouteProp<RootStackParamList, 'LobbyScreen'>;

type Props = {
    navigation: LobbyScreenNavigationProp;
    route: LobbyScreenRouteProp;
};

export default function LobbyScreen({ navigation, route }: Props) {
    const [localPlayerName, onChangeLocalPlayerName] = React.useState<string>('');
    const [otherPlayersNames, onChangeOtherPlayerNames] = React.useState<string[]>([]);
    const [numberOfPlayers, onChangeNumberOfPlayers] = React.useState<number>(1);

    let intervalRef: any = undefined;
    
    useEffect(() => {
        const unsubscribe = navigation.addListener('blur', () => {
            // screen is removed, stop background timers
            stopStateUpdate();
        });
        return unsubscribe;
    }, [navigation]);

    useEffect(() => {
        const subscribe = navigation.addListener('focus', async () => {
            // screen is focussed, stop background timers
            startStateUpdate();
        });
        return subscribe;
    }, [navigation]);

    const startStateUpdate = async () => {
        Log.info('starting state update interval task');
        intervalRef = setInterval(async () => {
            await updateState();
        }, 3000);
    }

    const stopStateUpdate = async () => {
        if (intervalRef) {
            Log.info('stopping state update interval task');
            clearInterval(intervalRef)
        }
    }

    const updateState = async () => {
        Log.info('updating state');
        await getGame(route.params!.gameID).then(async (game: Game) => {
            let playerNames: string[] = [];
            for (let i = 0; i < game.playerids.length; i++) {
                let playerID = game.playerids[i];
                await getPlayer(route.params!.gameID, playerID).then((player: Player) => {
                    if (player.ID != route.params!.localPlayerID) {
                        playerNames.push(player.name);
                    } else {
                        onChangeLocalPlayerName(player.name);
                    }
                }).catch((error: any) => {
                    Log.error(error);
                    openModal();
                });
            }
            if (playerNames.length != otherPlayersNames.length) {
                onChangeOtherPlayerNames(playerNames);
                onChangeNumberOfPlayers(game.playerids.length);
            }
            Log.info('updated game state on game ' + route.params!.gameID + '..waiting for players to join with invitecode ' + route.params!.inviteCode);
            if (game.status == GAME_STATE_RUNNING) {
                Log.info('game ' + route.params!.gameID + ' is running (probably started remotely), entering game screen..');
                navigation.navigate('GameScreen', { gameID: route.params!.gameID, localPlayerID: route.params!.localPlayerID, localPlayerName: localPlayerName });
            }
        }).catch((err) => {
            Log.error(err);
            openModal();
        });
    }

    const onPressShareButton = async () => {
        const result = await Share.share({
            message: 'Play a game of Rush for the Web with ' + route.params!.localPlayerName + '! Use invite code "' + route.params!.inviteCode + '".',
        });
    };

    const onPressStartButton = async () => {
        await stopStateUpdate();
        Log.info('starting game..');
        await startGame(route.params!.gameID).then(async (game: Game) => {
            Log.info('entering game..');
            navigation.navigate('GameScreen', { gameID: route.params!.gameID, localPlayerID: route.params!.localPlayerID, localPlayerName: localPlayerName });
        }).catch((error: any) => {
            Log.error(error);
            openModal();
        });
    };

    const openModal = () => {
        navigation.navigate('Modal', {
            title: 'Error occured!',
            description: 'An error has occured, please try again.',
            button1Text: 'Ok',
            onButton1Press: () => navigation.goBack()
        });
    }

    return (
        <ImageBackground source={background} style={{ flex: 1 }}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                <Image source={artwork} style={{ width: 350, height: 350, marginTop: 50 }} />
            </View>
            <View style={{ flex: 1 }}>
                <View style={{ flex: 1, width: '100%', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                    <Text style={{ fontFamily: 'HeaderFont', fontSize: normalize(14), color: '#324764' }}>Invite Code</Text>
                    <TextInput
                        style={{
                            width: '80%',
                            borderColor: '#444444',
                            borderWidth: 1,
                            textAlign: 'center',
                            marginBottom: 10,
                            fontFamily: 'TextFont',
                            color: '#324764',
                            backgroundColor: '#d8d8d8dd'
                        }}
                        editable={false}
                        value={route.params?.inviteCode! || ''}
                    />
                    <TitleButton title='Share' onPress={onPressShareButton} style={{ height: 40 }} />
                </View>
                <View style={{ flex: 0.2, width: '100%', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', marginBottom: 20 }}>
                    <Line size='large' />
                </View>
                <View style={{ flex: 2, width: '100%', flexDirection: 'column', alignItems: 'center', justifyContent: 'flex-start' }}>
                    <View style={{ width: '100%', flexDirection: 'column', alignItems: 'center', justifyContent: 'flex-start'}}>
                        <TextInput
                            style={{
                                width: '80%',
                                borderColor: '#324764',
                                borderWidth: 1,
                                textAlign: 'center',
                                marginBottom: 5,
                                fontFamily: 'TextFont',
                                color: '#324764',
                                backgroundColor: '#d8d8d8dd'
                            }}
                            editable={false}
                            value={route.params!.localPlayerName || ''}
                        />
                        {route.params!.localPlayerName == "" && <ActivityIndicator size="small" color="#888888" style={{ position: 'absolute', marginTop: 4 }}/>}
                    </View>
                    <View style={{ width: '100%', flexDirection: 'column', alignItems: 'center', justifyContent: 'flex-start'}}>
                        <TextInput
                            style={{
                                width: '80%',
                                borderColor: '#324764',
                                borderWidth: 1,
                                textAlign: 'center',
                                marginTop: 5,
                                marginBottom: 5,
                                fontFamily: 'TextFont',
                                color: '#324764',
                                backgroundColor: '#d8d8d8dd'
                            }}
                            editable={false}
                            value={otherPlayersNames[0] || ''}
                        />
                        {numberOfPlayers < 2 && <ActivityIndicator size="small" color="#888888" style={{ position: 'absolute', marginTop: 8 }} />}
                    </View>
                        <View style={{ width: '100%', flexDirection: 'column', alignItems: 'center', justifyContent: 'flex-start' }}>
                        <TextInput
                            style={{
                                width: '80%',
                                borderColor: '#324764',
                                borderWidth: 1,
                                textAlign: 'center',
                                marginTop: 5,
                                marginBottom: 5,
                                fontFamily: 'TextFont',
                                color: '#324764',
                                backgroundColor: '#d8d8d8dd'
                            }}
                            editable={false}
                            value={otherPlayersNames[1] || ''}
                        />
                        {numberOfPlayers < 3 && <ActivityIndicator size="small" color="#888888" style={{ position: 'absolute', marginTop: 8 }} />}
                    </View>
                    <View style={{ width: '100%', flexDirection: 'column', alignItems: 'center', justifyContent: 'flex-start' }}>
                        <TextInput
                            style={{
                                width: '80%',
                                borderColor: '#324764',
                                borderWidth: 1,
                                textAlign: 'center',
                                marginTop: 5,
                                marginBottom: 10,
                                fontFamily: 'TextFont',
                                color: '#324764',
                                backgroundColor: '#d8d8d8dd'
                            }}
                            editable={false}
                            value={otherPlayersNames[2] || ''}
                        />
                        {numberOfPlayers < 4 && <ActivityIndicator size="small" color="#888888" style={{ position: 'absolute', marginTop: 8 }} />}
                    </View>
                    <TitleButton title='Start Game' onPress={onPressStartButton} disabled={numberOfPlayers<2} style={{ height: 40 }} />
                </View>
            </View>
            <ViewHeader title='Game Lobby' onPressBack={() => navigation.goBack() }/>
        </ImageBackground>
    );
}
