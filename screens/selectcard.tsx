/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for select.
 */

import React from 'react';
import { ImageBackground, View } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';

import { CARD_TYPE_EVENT, CARD_TYPE_PERSONA, CARD_TYPE_PROJECT, CARD_TYPE_RESOURCE, normalize } from '../services/globals';
import { RootStackParamList } from '../App';
import { Card } from '../model/card';
import ViewHeader from '../components/viewheader';
import ProjectCard from '../components/projectcard';
import PersonaCard from '../components/personacard';
import EventCard from '../components/eventcard';
import ResourceCard from '../components/resourcecard';
import { getImageAssetById } from '../services/imageassets';

import background from '../assets/ui/background-screen.png';

type SelectCardScreenNavigationProp = StackNavigationProp<RootStackParamList, 'SelectCardScreen'>;
type SelectCardRouteProp = RouteProp<RootStackParamList, 'SelectCardScreen'>;

type Props = {
    navigation: SelectCardScreenNavigationProp;
    route: SelectCardRouteProp;
};

export default function SelectCardScreen({ navigation, route }: Props) {

    const headerTitle: string = route.params?.headerTitle!;
    const cards: Card[] = route.params?.cards!;
    const onSelectCard = route.params?.onSelectCard!;
    
    const renderCard = (card: Card) => {
        if (card) {
            switch (card.type) {
                case CARD_TYPE_EVENT:
                    return (<EventCard onPress={() => onTouchCard(card)} initialHidden={true} autoReveal={true} title={card.name[0]} titleSize={16} artwork={getImageAssetById(card.ID)} effectIcons={[card.effectid]} style={{ flex: 1, width: '90%', alignSelf: 'center' }} />);
                case CARD_TYPE_PERSONA:
                    return (<PersonaCard onPress={() => onTouchCard(card)} initialHidden={true} autoReveal={true} title={card.name[0]} titleSize={16} color={card.colors[0]} artwork={getImageAssetById(card.ID)} effectIcons={[card.effectid]} style={{ flex: 1, width: '90%', alignSelf: 'center' }} />);
                case CARD_TYPE_PROJECT:
                    return (<ProjectCard onPress={() => onTouchCard(card)} initialHidden={true} autoReveal={true} title={card.name[0]} colors={card.colors} payout={card.payout} artwork={getImageAssetById(card.ID)} effectIcons={[card.effectid]} style={{ flex: 1, width: '90%', height: '35%', alignSelf: 'center' }} titleStyle={{ fontSize: normalize(14), paddingTop: 5, paddingLeft: 5 }} sellStyle={{ fontSize: normalize(20) }} />);
                case CARD_TYPE_RESOURCE:
                    return (<ResourceCard onPress={() => onTouchCard(card)} initialHidden={true} autoReveal={true} title={card.name[0]} color={card.colors[0]} cost={card.cost} artwork={getImageAssetById(card.ID)} effectIcons={[card.effectid]} style={{ height: 230, width: 180 }} />);
            }
        }
        return null;
    }

    const onTouchCard = (card: Card) => {
        if (card) {
            onSelectCard(card);
            navigation.goBack();
        }
    }

    return (
        <ImageBackground source={background} style={{ flex: 1, paddingTop: 20 }}>
            <View
                style={{
                    flex: 1,
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}
            >
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-end', justifyContent: 'center' }}>
                    {cards.length > 0 && renderCard(cards[0])}
                    {cards.length > 1 && renderCard(cards[1])}
                </View>
                {cards.length > 2 &&
                    <View style={{ flex: 1, flexDirection: 'row', marginTop: 20, alignItems: 'flex-start', justifyContent: 'center'}}>
                    {cards.length > 0 && renderCard(cards[2])}
                    </View>
                }
            </View>
            <ViewHeader title={headerTitle} onPressBack={() => navigation.goBack()} />
        </ImageBackground>
    );
}
