/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React from 'react';
import { View, ImageBackground, SafeAreaView, ScrollView, FlatList, Image } from 'react-native';
import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';

import { RootStackParamList } from '../App';
import { Project } from '../model/project';
import { Card } from '../model/card';
import ResourceCard from '../components/resourcecard';
import ProjectCard from '../components/projectcard';
import ViewHeader from '../components/viewheader';
import { getImageAssetById } from '../services/imageassets';
import { getCardByID } from '../services/middleware';

import background from '../assets/ui/background-screen.png';
import emptyProject from '../assets/ui/empty-project-slot.png';
import { normalize } from '../services/globals';

type SelectProjectScreenNavigationProp = StackNavigationProp<RootStackParamList, 'SelectProjectScreen'>;
type SelectProjectRouteProp = RouteProp<RootStackParamList, 'SelectProjectScreen'>;

type Props = {
    navigation: SelectProjectScreenNavigationProp;
    route: SelectProjectRouteProp;
};

export default function SelectProjectScreen({ navigation, route }: Props) {

    const headerTitle: string = route.params?.headerTitle!;
    const card: Card = route.params?.card!;
    const projects: Project[] = route.params?.projects!;
    const onSelectProject = route.params?.onSelectProject!;

    const renderProject = (project: Project | undefined) => {
        if (!project) {
            return (
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                    <View style={{ marginRight: 10 }}>
                        <Image source={emptyProject} resizeMode='contain' style={{ width: 100, height: 140 }} />
                    </View>
                </View>
            )            
        }
        let projectCard: Card | undefined = getCardByID(project.cardid);
        let cards : Card[] = [];
        for (const cardID of project.resourcecardids) {
            let card: Card | undefined = getCardByID(cardID);
            if (card) {
                cards.push(card);
            }
        }

        const projectCardTouched = (project: Project) => {
            if (onSelectProject) {
                onSelectProject(project);
            }
            navigation.goBack();
        }

        return (
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <View style={{ marginRight: 10 }}>
                    <ProjectCard title={projectCard?.name[0]!} onPress={() => projectCardTouched(project)} artwork={getImageAssetById(projectCard?.illustrationid!)} colors={projectCard?.colors!} payout={projectCard?.payout!} initialHidden={false} autoReveal={false} style={{ width: 100, height: 140 }} titleStyle={{ fontSize: normalize(10), paddingTop: 1, paddingLeft: 4 }} sellStyle={{ fontSize: normalize(12) }}/>
                </View>
                <SafeAreaView>
                    <ScrollView style={{ marginRight: 10 }}>
                        <FlatList
                            horizontal
                            data={cards}
                            renderItem={({ item }) => {
                                return (
                                    <ResourceCard title={item.name[0]} artwork={0} cost={0} color={''} initialHidden={false} autoReveal={false} style={{ width: 100, height: 140 }} />
                                )
                            }}
                            showsHorizontalScrollIndicator={false}
                        />
                    </ScrollView>
                </SafeAreaView>
            </View>
        );
    };

    return (
        <ImageBackground source={background} style={{ flex: 1, paddingTop: 110 }}>
            <View
                style={{
                    flex: 1,
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    justifyContent: 'center',
                    marginLeft: 15
                }}
            >
                {projects?.length! > 0 ? renderProject(projects![0]) : renderProject(undefined)}
                {projects?.length! > 1 ? renderProject(projects![1]) : renderProject(undefined)}
                {projects?.length! > 2 ? renderProject(projects![2]) : renderProject(undefined)}
            </View>
            <ViewHeader title={headerTitle} onPressBack={() => navigation.goBack()} />
        </ImageBackground>
    );
}
