/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React from 'react';
import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import { View, Text } from 'react-native';

import { RootStackParamList } from '../App';
import TitleButton from '../components/titlebutton';

type ModalScreenNavigationProp = StackNavigationProp<RootStackParamList, 'Modal'>;
type ModalScreenRouteProp = RouteProp<RootStackParamList, 'Modal'>;

type Props = {
    navigation: ModalScreenNavigationProp;
    route: ModalScreenRouteProp;
};

export default ({ navigation, route }: Props) => (
    <View
        style={{
            flex: 1,
            backgroundColor: 'transparent',
            alignItems: 'center',
            justifyContent: 'center',
        }}
    >

        <View style={{ width: '80%', padding: 20, backgroundColor: '#bcbcbc', borderColor: '#b2b2b2', borderWidth: 1, borderRadius: 10 }}>
            <Text style={{ fontFamily: 'HeaderFont', color: 'white', fontSize: 18, textAlign: 'center' }}>{route.params?.title}</Text>
            <Text style={{ fontFamily: 'TextFont', color: 'white', fontSize: 14, textAlign: 'center', marginTop: 10, paddingHorizontal: 15 }}>{route.params?.description}</Text>
            <View style={{ alignItems: 'center', marginTop: 10 }}>
                <TitleButton title={route.params?.button1Text || ""} onPress={(event) => route.params?.onButton1Press(event)} style={{ height: 40, marginTop: 10 }} />
                {route.params?.button2Text &&
                    <TitleButton title={route.params?.button2Text || ""} onPress={(event) => route.params?.onButton2Press!(event)} style={{ height: 40, marginTop: 10 }} />
                }
            </View>
        </View>
    </View>
);