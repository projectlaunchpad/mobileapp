/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React from 'react';
import { GestureResponderEvent, Image, ImageBackground, ImageSourcePropType, SafeAreaView, ScrollView, StyleSheet, View } from 'react-native';
import { RouteProp } from '@react-navigation/native';
import { StackNavigationProp } from '@react-navigation/stack';
import Markdown from 'react-native-markdown-display';

import { RootStackParamList } from '../App';
import TitleLarge from '../components/titlelarge';
import ViewHeader from '../components/viewheader';

import background from '../assets/ui/background-screen.png';
import TitleButton from '../components/titlebutton';

type DetailTextScreenNavigationProp = StackNavigationProp<RootStackParamList, 'DetailTextScreen'>;
type DetailTextRouteProp = RouteProp<RootStackParamList, 'DetailTextScreen'>;

type Props = {
    navigation: DetailTextScreenNavigationProp;
    route: DetailTextRouteProp;
};

export default function DetailTextScreen({ navigation, route }: Props) {

    const headerTitle: string = route.params?.headerTitle!;
    const title: string = route.params?.title!;
    const image: ImageSourcePropType | undefined = route.params?.image;
    const text: string = route.params?.text!;
    const button1Text: string = route.params?.button1Text!;
    const onButton1Press: ((event: GestureResponderEvent) => void) = route.params?.onButton1Press!;
    const button2Text: string = route.params?.button2Text!;
    const onButton2Press: ((event: GestureResponderEvent) => void) = route.params?.onButton2Press!;

    const onLinkPress = (url: string) => {
        // return true to open with Linking.openURL()
        // return false to handle it locally
        console.log('touch on ' + url);
        if (url && url.startsWith('compendium:')) {
            // FIXME: implement some custom link logic
            return false;
        }
        return true;
    };

    return (
        <ImageBackground source={background} style={{ flex: 1, paddingTop: 20 }}>
            <View
                style={{
                    flex: 1,
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    marginTop: 50,
                }}
            >
                {image && (
                    <View
                        style={{
                            flex: 5,
                            width: '100%',
                            alignItems: 'center',
                            justifyContent: 'center',
                        }}
                    >
                        <Image style={{ height: 300, width: 300 }} source={image} resizeMode='contain' />
                    </View>
                )}
                <View style={{ flex: 1, width: '100%', justifyContent: 'center', alignItems: 'center' }}>
                    <TitleLarge style={{ height: 50 }} title={title} />
                </View>
                <SafeAreaView style={{ flex: 10, width: '100%', paddingTop: 10 }}>
                    <ScrollView style={{ marginHorizontal: 20 }} contentInsetAdjustmentBehavior='automatic'>
                        <Markdown style={markdownStyles} onLinkPress={onLinkPress}>
                            {text}
                        </Markdown>
                    </ScrollView>
                </SafeAreaView>
                <View style={{ flex: 1, width: '100%', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginBottom: 20 }}>
                    { button1Text &&
                        <TitleButton title={button1Text || ""} onPress={ (event) => onButton1Press!(event) } style={{ height: 40, marginTop: 10 }}/>
                    }
                    { button2Text &&
                        <TitleButton title={button2Text || ""} onPress={ (event) => onButton2Press!(event) } style={{ height: 40, marginTop: 10 }}/>
                    }
                </View>
            </View>

             <ViewHeader title={headerTitle} onPressBack={() => navigation.goBack()} />
        </ImageBackground>
    );
}

const markdownStyles: StyleSheet.NamedStyles<any> | undefined = {
    body: {
        fontFamily: 'TextFont',
        fontSize: 14,
        color: 'white',
    },
    heading1: {
        fontSize: 18,
    },
    heading2: {
        fontSize: 16,
    },
    heading3: {
        fontSize: 14,
        fontWeight: 'bold',
    },
    link: {
        textDecorationLine: 'underline',
    },
    mailTo: {
        textDecorationLine: 'underline',
    },
    text: {
        textAlign: 'justify',
    },
};
