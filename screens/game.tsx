/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React, { useEffect } from 'react';
import { Text, View, ImageBackground, SafeAreaView, ScrollView, FlatList, LogBox } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import { RouteProp } from '@react-navigation/native';
import Spinner from 'react-native-loading-spinner-overlay';

import { RootStackParamList } from '../App';
import { CARD_TYPE_EVENT, CARD_TYPE_PROJECT, CARD_TYPE_RESOURCE, GAME_STATE_RUNNING, normalize } from '../services/globals';
import { Game } from '../model/game';
import { Player } from '../model/player';
import { Stack } from '../model/stack';
import { Hand } from '../model/hand';
import { Card } from '../model/card';
import { Project } from '../model/project';
import { Action } from '../model/action';
import TitleButton from '../components/titlebutton';
import ViewHeader from '../components/viewheader';
import ResourceCard from '../components/resourcecard';
import CardStack from '../components/cardstack';
import { Log } from '../services/log';
import { getImageAssetById, getPersonaFrontById } from '../services/imageassets';
import { acceptProjectCard, discardResourceToPool, drawProjectCards, drawResourceFromDeck, endTurn, getAction, getCardByID, getGame, getHand, getPlayer, getProject, getResourceFromPool, getStack, playResourceToProject, sellProject } from '../services/middleware';
import PlayerScreen from './player';

import background from '../assets/ui/background-screen.png';
import PersonaCard from '../components/personacard';

export type GameScreenNavigationProp = StackNavigationProp<RootStackParamList, 'GameScreen'>;
type GameScreenRouteProp = RouteProp<RootStackParamList, 'GameScreen'>;

type Props = {
    navigation: GameScreenNavigationProp;
    route: GameScreenRouteProp;
};

const Tab = createMaterialTopTabNavigator();

export default function GameScreen({ navigation, route }: Props) {
    const [gameState, onChangeGameState] = React.useState<Game>();
    const [onTurn, onChangeOnTurn] = React.useState<boolean>(false);
    const [players, onChangePlayers] = React.useState<Player[]>([]);
    const [playerProjects, onChangePlayerProjects] = React.useState<Record<string, Project[]>>({});
    const [localPlayerHand, onChangeLocalPlayerHand] = React.useState<Hand>();
    const [localPlayerPersonaCard, onChangeLocalPlayerPersonaCard] = React.useState<Card>();
    const [lastAction, onChangeLastAction] = React.useState<Action>();
    const [handCards, onChangeHandCards] = React.useState<Card[]>();
    const [poolStack, onChangePoolStack] = React.useState<Stack>();
    const [resourceStack, onChangeResourceStack] = React.useState<Stack>();
    const [projectStack, onChangeProjectStack] = React.useState<Stack>();
    const [eventStack, onChangeEventStack] = React.useState<Stack>();
    const [spinnerVisible, setSpinnerVisible] = React.useState<boolean>(true);

    let intervalRef: any = undefined;

    LogBox.ignoreLogs([
        'Non-serializable values were found in the navigation state',
    ]);
    
    useEffect(() => {
        const unsubscribe = navigation.addListener('blur', () => {
            // screen is removed, stop background timers
            stopStateUpdate();
        });
        return unsubscribe;
    }, [navigation]);

    useEffect(() => {
        const subscribe = navigation.addListener('focus', async () => {
            // screen is focussed, stop background timers
            startStateUpdate();
        });
        return subscribe;
    }, [navigation]);

    const startStateUpdate = async () => {
        Log.info('starting state update interval task');
        intervalRef = setInterval(async () => {
            await updateState();
            setSpinnerVisible(false);
        }, 5000);
    }

    const stopStateUpdate = async () => {
        if (intervalRef) {
            Log.info('stopping state update interval task');
            clearInterval(intervalRef)
        }
    }

    const updateProjects = async (player: Player) => {
        Log.info('updating projects for player ' + player.ID);
        let projects: Project[] = [];
        for (let i = 0; i < player.projectids.length; i++) {
            let projectID = player.projectids[i];
            await getProject(player.gameid, projectID).then(async (project: Project) => {
                Log.info('updated project ' + project.ID + ' for player ' + player.ID + ' with ' + project.resourcecardids.length + ' resources');
                projects.push(project);
            }).catch((error: any) => {
                Log.error(error);
                errorModal();
            });
        }
        return projects;
    }

    const updateState = async () => {
        Log.info('updating state for game id ' + route.params!.gameID);
        await getGame(route.params!.gameID).then(async (game: Game) => {
            onChangeGameState(game);
            Log.info('updated game state on game ' + route.params!.gameID);
            if (game.currentplayerid == route.params!.localPlayerID) {
                onChangeOnTurn(true);
                Log.info('player ' + route.params!.localPlayerID + ' is on turn');
            } else {
                onChangeOnTurn(false);
                Log.info('player ' + route.params!.localPlayerID + ' is NOT on turn');
            }
            await getStack(game.ID, game.poolstackid).then(stack => onChangePoolStack(stack)).catch((error) => {
                Log.error(error);
                errorModal();
            });
            await getStack(game.ID, game.resourcestackid).then(stack => onChangeResourceStack(stack)).catch((error) => {
                Log.error(error);
                errorModal();
            });
            await getStack(game.ID, game.projectstackid).then(stack => onChangeProjectStack(stack)).catch((error) => {
                Log.error(error);
                errorModal();
            });
            await getStack(game.ID, game.eventstackid).then(stack => onChangeEventStack(stack)).catch((error) => {
                Log.error(error);
                errorModal();
            });
            Log.info('updated game stacks on game ' + route.params!.gameID);
            if (game.actionids.length == 1) {
                await getAction(game.ID, game.eventstackid).then(action => onChangeLastAction(action)).catch((error) => {
                    Log.error(error);
                    errorModal();
                });
                Log.info('updated last action on game ' + route.params!.gameID);
            } else {
                Log.info('invalid action log on game ' + route.params!.gameID + ' action length is ' + game.actionids?.length);
            }

            let players: Player[] = [];
            let playerProjects: Record<string, Project[]> = {};
            for (let i = 0; i < game.playerids.length; i++) {
                let playerID = game.playerids[i];
                await getPlayer(route.params!.gameID, playerID).then(async (player: Player) => {
                    playerProjects[player.ID] = await updateProjects(player);
                    if (playerID == route.params!.localPlayerID) {
                        onChangeLocalPlayerPersonaCard(getCardByID(player.personacardid));
                        await getHand(game.ID, player.handid).then(hand => {
                            Log.info('updated hand for player ' + player.handid);
                            onChangeLocalPlayerHand(hand);
                            let handCards: Card[] = [];
                            for (let j = 0; j < hand.cardids.length; j++) {
                                let cardID = hand.cardids[j];
                                let card = getCardByID(cardID);
                                if (card) {
                                    handCards.push(card);
                                }
                            }
                            onChangeHandCards(handCards);
                        }).catch((error) => {
                            Log.error(error);
                            errorModal();
                        });
                    };
                    players.push(player);
                }).catch((error: any) => {
                    Log.error(error);
                    errorModal();
                });
            }
            Log.info('updated players on game ' + route.params!.gameID + ', player count is ' + players.length);
            onChangePlayers(players);
            onChangePlayerProjects(playerProjects);
            if (game.status != GAME_STATE_RUNNING) {
                Log.info('game ' + route.params!.gameID + ' has ended, entering resume screen..');
                navigation.navigate('ResumeScreen', { gameID: route.params!.gameID, localPlayerID: route.params!.localPlayerID });
            }
        }).catch((err) => {
            Log.error(err);
            errorModal();
        });
    }

    const errorModal = () => {
        navigation.navigate('Modal', {
            title: 'Error occured!',
            description: 'An error has occured, please try again.',
            button1Text: 'Ok',
            onButton1Press: () => navigation.goBack()
        });
    }

    const confirmModal = (text: string, callback: () => void) => {
        navigation.navigate('Modal', {
            title: 'Please confirm!',
            description: text,
            button1Text: 'Ok',
            onButton1Press: () => {
                navigation.goBack();
                callback();
            },
            button2Text: 'Cancel',
            onButton2Press: () => navigation.goBack()
        });
    }

    const onSelectProject = (card: Card, callback: (project: Project | undefined) => void) => {
        navigation.navigate('SelectProjectScreen', {
            headerTitle: 'Select Project',
            card: card!,
            projects: playerProjects[players[0]?.ID],
            onSelectProject: (project: Project) => callback(project)
        });
    }

    const onPlayToProject = (card: Card, project: Project) => {
        Log.info('playing card ' + card.ID + ' to project ' + project.ID);
        playResourceToProject(route.params!.gameID, project.ID, card.ID).then(() => {
            Log.info('played card ' + card.ID + ' to project ' + project.ID);
        }).catch((error: any) => {
            Log.error(error);
            errorModal();
        });
    }

    const onPlayToPool = (card: Card | undefined) => {
        if (card) {
            Log.info('playing card ' + card.ID + ' to pool');
            discardResourceToPool(route.params!.gameID, card.ID).then(() => {
                Log.info('played card ' + card.ID + ' to pool');
            }).catch((error: any) => {
                Log.error(error);
                errorModal();
            });
        }
    }

    const onShowHandCard = (cardID: string) => {
        let card: Card | undefined = getCardByID(cardID);
        if (card) {
            navigation.navigate('DetailCardScreen', {
                headerTitle: 'Hand Card',
                card: card!,
                initialHidden: false,
                autoReveal: false,
                button1Text: 'Play to Project',
                button1Disabled: !onTurn,
                onButton1Press: () => {
                    onSelectProject(card!, (project: Project | undefined) => {
                        if (project) {
                            onPlayToProject(card!, project);
                        }
                    });
                },
                button2Text: 'Discard to Pool',
                button2Disabled: !onTurn,
                onButton2Press: () => {
                    onPlayToPool(card);
                },
            });
        }
    }

    const onShowProjectCardFromLocalPlayer = (cardID: string) => {
        let card = getCardByID(cardID);
        navigation.navigate('DetailCardScreen', {
            headerTitle: 'Card Detail',
            card: card!,
            initialHidden: false,
            autoReveal: false,
            button1Text: 'Sell Project',
            button1Disabled: !onTurn,
            onButton1Press: () => {
                let project: Project | undefined = undefined;
                for (let i = 0; i < playerProjects[players[0]?.ID].length; i++) {            
                    let p = playerProjects[players[0]?.ID][i];
                    if (p.cardid == cardID) {
                        project = p;
                        break;
                    }
                }
                if (project) {
                    confirmModal('Sell this project?', () => {
                        sellProject(route.params!.gameID, project!.ID).then((player: Player) => {
                            Log.info('sold project, player credits is now ' + player.credits);
                        }).catch((error) => {
                            Log.error(error);
                            errorModal();
                        });
                    });
                }
            }
        });
    }

    const onShowProjectCardFromOtherPlayer = (cardID: string) => {
        let card = getCardByID(cardID);
        navigation.navigate('DetailCardScreen', {
            headerTitle: 'Card Detail',
            card: card!,
            initialHidden: false,
            autoReveal: false
        });
    }

    const onShowResourceCardFromLocalProject = (cardID: string) => {
        let card = getCardByID(cardID);
        navigation.navigate('DetailCardScreen', {
            headerTitle: 'Card Detail',
            card: card!,
            initialHidden: false,
            autoReveal: false
        });
    }

    const onShowPersonaCard = (cardID: string) => {
        let card = getCardByID(cardID);
        navigation.navigate('DetailCardScreen', {
            headerTitle: 'Card Detail',
            card: card!,
            initialHidden: false,
            autoReveal: false
        });
    }

    const onShowResourceCardFromOtherPlayersProject = (cardID: string) => {
        let card = getCardByID(cardID);
        navigation.navigate('DetailCardScreen', {
            headerTitle: 'Card Detail',
            card: card!,
            initialHidden: false,
            autoReveal: false
        });
    }

    const onTouchPoolStack = () => {
        let card = getCardByID(poolStack?.cardids![0]!);
        navigation.navigate('DetailCardScreen', {
            headerTitle: 'Card Detail',
            card: card!,
            initialHidden: false,
            autoReveal: false,
            button1Text: 'Aquire',
            button1Disabled: !onTurn,
            onButton1Press: () => {
                confirmModal('Aquire this card?', () => {
                    getResourceFromPool(route.params!.gameID).then((card: Card) => {
                        Log.info('acquired resource from pool, resource card id ' + card.ID);
                    }).catch((error) => {
                        Log.error(error);
                        errorModal();
                    });
                });
            }
        });
    }

    const onTouchEventStack = () => {
        let card = getCardByID(gameState?.currenteventcardid!);
        navigation.navigate('DetailCardScreen', {
            headerTitle: 'Card Detail',
            card: card!,
            initialHidden: false,
            autoReveal: false
        });
    }

    const onTouchResourceStack = () => {
        if (onTurn)
            confirmModal('Ok to draw from resources?', () => { 
                Log.info('drawing from resources');
                drawResourceFromDeck(route.params!.gameID).then((card: Card) => {
                    Log.info('drawing from resources success, got card ' + card.ID);    
                    navigation.navigate('DetailCardScreen', {
                        headerTitle: 'Drawn Card',
                        card: card!,
                        initialHidden: true,
                        autoReveal: true
                    });
                }).catch((error) => {
                    Log.error(error);
                    errorModal();
                });
            });
    }

    const onTouchProjectStack = () => {
        if (onTurn)
            confirmModal('Ok to draw from projects?', () => {
                Log.info('drawing from projects');
                drawProjectCards(route.params!.gameID).then((cards: Card[]) => {
                    Log.info('drawing from projects success, got ' + cards.length + ' cards');
                    navigation.navigate('SelectCardScreen', {
                        headerTitle: 'Select Project Card',
                        cards: cards,
                        onSelectCard: (card: Card) => {
                            Log.info('selected project card ' + card.ID);
                            acceptProjectCard(route.params!.gameID, card.ID).then((player: Player) => {
                                Log.info('accepted project card ' + card.ID);
                            }) .catch((error) => {
                                Log.error(error);
                                errorModal();
                            });
                        }
                    });
                }).catch((error) => {
                    Log.error(error);
                    errorModal();
                });
            });
    }

    const onGiveCard = () => {
        console.log('action');
        // FIXME: implement
    }

    const onGiveCredits = () => {
        console.log('action');
        // FIXME: implement
    }

    const onEndTurn = async () => {
        navigation.navigate('Modal', {
            title: 'Please confirm!',
            description: 'Ok to end your turn?',
            button1Text: 'Ok',
            onButton1Press: async () => {
                await endTurn(route.params!.gameID).then(async (game: Game) => {
                    Log.info('ended turn on game ' + route.params!.gameID);
                    if (game.currentplayerid == route.params!.localPlayerID) {
                        onChangeOnTurn(true);
                        Log.info('player ' + route.params!.localPlayerID + ' is on turn');
                    } else {
                        onChangeOnTurn(false);
                        Log.info('player ' + route.params!.localPlayerID + ' is NOT on turn');
                    }
                }).catch((err) => {
                    Log.error(err);
                    errorModal();
                });
                navigation.goBack();
            },
            button2Text: 'Cancel',
            onButton2Press: () => navigation.goBack()
        });
    }

    const onEndGame = () => {
        // FIXME: implement
        console.log('action');
    }

    return (
        <ImageBackground source={background} style={{ flex: 1, paddingTop: 20 }}>
            <Spinner
                visible={spinnerVisible}
                cancelable={false}
                overlayColor={'rgba(0, 0, 0, 0.7)'}
                textContent={''}
            />
            <View style={{ flex: 3 }}>
                <Tab.Navigator
                    style={{ marginTop: 50 }}
                    screenOptions={{
                        tabBarLabelStyle: {
                            fontSize: 12,
                            color: '#324764',
                            fontFamily: 'TextFont'
                        },
                        tabBarStyle: {
                            backgroundColor: 'transparent',
                        },
                        tabBarIndicatorStyle: {
                            backgroundColor: '#ff7617'
                        }
                    }}
                >
                    <Tab.Screen name={route.params!.localPlayerName} children={() =>
                        <PlayerScreen player={players[0] || undefined} projects={playerProjects[players[0]?.ID]} onPressProjectCard={(cardID: string) => onShowProjectCardFromLocalPlayer(cardID)} onPressResourceCard={(cardID: string) => onShowResourceCardFromLocalProject(cardID)}/>
                    } />
                    {players.length > 1 &&
                        <Tab.Screen name={players[1]?.name || 'Player 2'} children={() =>
                        <PlayerScreen player={players[1] || undefined} projects={playerProjects[players[1]?.ID]} onPressProjectCard={(cardID: string) => onShowProjectCardFromOtherPlayer(cardID)} onPressResourceCard={(cardID: string) => onShowResourceCardFromOtherPlayersProject(cardID)}/>
                        } />
                    }
                    {players.length > 2 && 
                        <Tab.Screen name={players[2]?.name || 'Player 3'} children={() =>
                        <PlayerScreen player={players[2] || undefined} projects={playerProjects[players[2]?.ID]} onPressProjectCard={(cardID: string) => onShowProjectCardFromOtherPlayer(cardID)} onPressResourceCard={(cardID: string) => onShowResourceCardFromOtherPlayersProject(cardID)} />
                        } />
                    }
                    {players.length > 3 &&
                        <Tab.Screen name={players[3]?.name || 'Player 4'} children={() =>
                        <PlayerScreen player={players[3] || undefined} projects={playerProjects[players[3]?.ID]} onPressProjectCard={(cardID: string) => onShowProjectCardFromOtherPlayer(cardID)} onPressResourceCard={(cardID: string) => onShowResourceCardFromOtherPlayersProject(cardID)} />
                        } />
                    }
                </Tab.Navigator>
            </View>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start' }}>
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'center', marginTop: 20 }}>
                    {localPlayerPersonaCard && <PersonaCard onPress={() => onShowPersonaCard(localPlayerPersonaCard?.ID)} initialHidden={false} autoReveal={false} title={localPlayerPersonaCard?.name[0]!} artwork={getPersonaFrontById(localPlayerPersonaCard?.illustrationid!)} effectIcons={[localPlayerPersonaCard?.effectid!]} effectArgs={localPlayerPersonaCard?.effectargs!} style={{ width: 42, height: 60, marginRight: 20 }} titleStyle={{ fontSize: normalize(6), paddingTop: 1, paddingLeft: 3 }} effectStyle={{ fontSize: normalize(6) }}/> }
                    <CardStack onPress={() => onTouchEventStack()} name={'Events'} fontSize={10} cardFontSize={5} cardsLeft={eventStack?.size!} type={CARD_TYPE_EVENT} openCard={getCardByID(gameState?.currenteventcardid!)} style={{ width: 42, height: 60, marginRight: 50 }} />
                    <CardStack onPress={() => onTouchResourceStack()} name={'Resources'} fontSize={10} cardFontSize={5} cardsLeft={resourceStack?.size!} type={CARD_TYPE_RESOURCE} style={{ width: 42, height: 60 }}/>
                    <CardStack onPress={() => onTouchProjectStack()} name={'Projects'} fontSize={10} cardFontSize={5} cardsLeft={projectStack?.size!} type={CARD_TYPE_PROJECT} style={{ width: 42, height: 60, marginLeft: 20 }} />
                    <CardStack onPress={() => onTouchPoolStack()} name={'Pool'} fontSize={10} cardFontSize={5} cardsLeft={poolStack?.size!} type={CARD_TYPE_RESOURCE} openCard={getCardByID(poolStack?.cardids![0]!)} style={{ width: 42, height: 60, marginLeft: 20 }} />
                </View>
            </View>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start' }}>
                <SafeAreaView>
                    <ScrollView >
                        <FlatList
                            horizontal
                            data={handCards}
                            keyExtractor={item => item.ID}
                            renderItem={({ item }) => {
                                return (
                                    <ResourceCard onPress={() => onShowHandCard(item.ID)} title={item?.name[0]!} artwork={getImageAssetById(item?.illustrationid!)} cost={item?.cost!} color={item?.colors[0]!} initialHidden={false} autoReveal={false} style={{ width: 70, height: 100, marginHorizontal: 5 }} titleStyle={{ fontSize: normalize(6), paddingTop: 2.5, paddingLeft: 5 }}/>
                            )}}
                            showsHorizontalScrollIndicator={false}
                        />
                    </ScrollView>
                </SafeAreaView>
            </View>
            <View style={{ flex: 0.3, alignItems: 'center', justifyContent: 'center', borderColor: '#324764', borderWidth: 1, borderRadius: 5, marginHorizontal: 5, marginVertical: 5 }}>
                {lastAction && <Text numberOfLines={1} style={{ fontFamily: 'TextFont', color: '#324764' }}>{lastAction.name}</Text>}
            </View>
            <View style={{ flex: 0.3, alignItems: 'center', justifyContent: 'center', borderColor: '#324764', borderWidth: 1, borderRadius: 5, marginHorizontal: 5, marginVertical: 5 }}>
                {gameState?.currentplayerid == route.params!.localPlayerID && <Text numberOfLines={1} style={{ fontFamily: 'TextFont', color: '#324764' }}>YOUR TURN</Text>}
                {gameState?.currentplayerid != route.params!.localPlayerID && <Text numberOfLines={1} style={{ fontFamily: 'TextFont', color: '#324764' }}>OTHER PLAYER'S TURN</Text>}
            </View>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'flex-start' }}>
                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'flex-start' }}>
                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'flex-start' }}>
                        <TitleButton title={'End Turn'} onPress={() => onEndTurn()} disabled={!onTurn} style={{ height: 40, marginTop: 10 }} />
                    </View>
                    <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'flex-start' }}>
                        <TitleButton title={'Give Credits'} onPress={() => onGiveCredits()} disabled={!onTurn} style={{ height: 40, marginTop: 10 }} />
                    </View>
                </View>
            </View>
            <ViewHeader title="Game" onPressBack={() => onEndGame()} />
        </ImageBackground>
    );
}

