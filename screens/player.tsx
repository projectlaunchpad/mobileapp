/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React from 'react';
import { Text, View, ImageBackground, SafeAreaView, ScrollView, FlatList, Image } from 'react-native';

import { Project } from '../model/project';
import { Card } from '../model/card';
import { Player } from '../model/player';
import ResourceCard from '../components/resourcecard';
import { getImageAssetById } from '../services/imageassets';
import ProjectCard from '../components/projectcard';

import background from '../assets/ui/background-player.png';
import iconProjectCards from '../assets/ui/icon-projectcards.png';
import iconCredits from '../assets/ui/icon-credits.png';
import emptyProject from '../assets/ui/empty-project-slot.png';
import { getCardByID } from '../services/middleware';
import { normalize } from '../services/globals';

type Props = {
    player: Player | undefined;
    projects: Project[] | undefined;
    onPressProjectCard?: ((cardID: string) => void) | undefined
    onPressResourceCard?: ((cardID: string) => void) | undefined
};

export default function PlayerScreen({ player, projects, onPressProjectCard, onPressResourceCard }: Props) {

    const renderProject = (project: Project | undefined) => {
        if (!project) {
            return (
                <View style={{ flex: 1, flexDirection: 'row' }}>
                    <View style={{ marginRight: 10 }}>
                        <Image source={emptyProject} resizeMode='contain' style={{ width: 50, height: 70 }} />
                    </View>
                </View>
            )            
        }
        let projectCard: Card | undefined = getCardByID(project.cardid);
        let cards : Card[] = [];
        for (const cardID of project.resourcecardids) {
            let card: Card | undefined = getCardByID(cardID);
            if (card) {
                cards.push(card);
            }
        }

        const projectCardTouched = (cardID: string) => {
            if (onPressProjectCard) {
                onPressProjectCard(cardID);
            }
        }

        const resourceCardTouched = (cardID: string) => {
            if (onPressResourceCard) {
                onPressResourceCard(cardID);
            }
        }

        return (
            <View style={{ flex: 1, flexDirection: 'row' }}>
                <View style={{ marginRight: 10 }}>
                    <ProjectCard title={projectCard?.name[0]!} onPress={() => projectCardTouched(projectCard?.ID!)} artwork={getImageAssetById(projectCard?.illustrationid!)} colors={projectCard?.colors!} payout={projectCard?.payout!} initialHidden={false} autoReveal={false} style={{ width: 50, height: 70 }} titleStyle={{ fontSize: normalize(6), paddingTop: 1, paddingLeft: 3 }} sellStyle={{ fontSize: normalize(6) }} />
                </View>
                <SafeAreaView>
                    <ScrollView >
                        <FlatList
                            horizontal
                            data={cards}
                            renderItem={({ item }) => {
                                return (
                                    <ResourceCard title={item.name[0]} onPress={() => resourceCardTouched(projectCard?.ID!)} artwork={0} cost={0} color={''} initialHidden={false} autoReveal={false} style={{ width: 50, height: 70 }} />
                                )
                            }}
                            showsHorizontalScrollIndicator={false}
                        />
                    </ScrollView>
                </SafeAreaView>
            </View>
        );
    };

    return (
        <ImageBackground resizeMode={'stretch'} source={background} style={{ flex: 1, paddingTop: 10 }}>
            <View
                style={{
                    flex: 1,
                    flexDirection: 'column',
                    alignItems: 'flex-start',
                    justifyContent: 'center',
                    marginLeft: 15
                }}
            >
                <View style={{ flex: 0.7, flexDirection: 'row', marginLeft: 5, alignItems: 'flex-start', justifyContent: 'flex-start' }}>
                    <View style={{ flex: 1, flexDirection: 'column'}}>
                        {player?.isfirstplayer && <Text style={{ fontFamily: 'TextFont', fontSize: 10, color: '#324764', marginTop: 3 }}>Starting Player</Text>}
                    </View>
                    <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'flex-end', marginRight: 20 }}>
                        <Image source={iconProjectCards} style={{ width: 20, height: 20 }}/>
                        <Text style={{ fontFamily: 'TextFont', fontSize: 13, color: '#324764', marginLeft: 3 }}>{player?.projectids.length}</Text>
                        <Image source={iconCredits} style={{ width: 20, height: 20, marginLeft: 20 }} />
                        <Text style={{ fontFamily: 'TextFont', fontSize: 13, color: '#324764', marginLeft: 3 }}>{player?.credits}</Text>
                    </View>
                </View>
                <View style={{ flex: 6, paddingBottom: 5 }}>
                    {projects?.length! > 0 ? renderProject(projects![0]) : renderProject(undefined)}
                    {projects?.length! > 1 ? renderProject(projects![1]) : renderProject(undefined)}
                    {projects?.length! > 2 ? renderProject(projects![2]) : renderProject(undefined)}
                </View>
            </View>
        </ImageBackground>
    );
}
