/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React from 'react';
import { GestureResponderEvent, ImageBackground, StyleProp, View, ViewStyle } from 'react-native';
import { StackNavigationProp } from '@react-navigation/stack';
import { RouteProp } from '@react-navigation/native';

import { RootStackParamList } from '../App';
import { CARD_TYPE_EVENT, CARD_TYPE_PERSONA, CARD_TYPE_PROJECT, CARD_TYPE_RESOURCE, normalize } from '../services/globals';
import { Card } from '../model/card';
import ViewHeader from '../components/viewheader';
import ProjectCard from '../components/projectcard';
import PersonaCard from '../components/personacard';
import EventCard from '../components/eventcard';
import ResourceCard from '../components/resourcecard';
import TitleButton from '../components/titlebutton';
import { getImageAssetById, getPersonaFrontById } from '../services/imageassets';

import background from '../assets/ui/background-screen.png';

type DetailCardScreenNavigationProp = StackNavigationProp<RootStackParamList, 'DetailCardScreen'>;
type DetailCardRouteProp = RouteProp<RootStackParamList, 'DetailCardScreen'>;

type Props = {
    navigation: DetailCardScreenNavigationProp;
    route: DetailCardRouteProp;
};

export default function DetailCardScreen({ navigation, route }: Props) {

    const headerTitle: string = route.params?.headerTitle!;
    const card: Card = route.params?.card!;
    const initialHidden = route.params?.initialHidden!;
    const autoReveal = route.params?.autoReveal!;
    const button1Text: string | undefined = route.params?.button1Text;
    const button2Text: string | undefined = route.params?.button2Text;
    const button3Text: string | undefined = route.params?.button3Text;
    const button4Text: string | undefined = route.params?.button4Text;
    const button1Disabled: boolean | undefined = route.params?.button1Disabled;
    const button2Disabled: boolean | undefined = route.params?.button1Disabled;
    const button3Disabled: boolean | undefined = route.params?.button1Disabled;
    const button4Disabled: boolean | undefined = route.params?.button1Disabled;

    const onPress = (event: GestureResponderEvent, callback: ((event: GestureResponderEvent) => void) | undefined) => {
        navigation.goBack();
        if (callback) {
            callback(event);
        }
    }

    return (
        <ImageBackground source={background} style={{ flex: 1, paddingTop: 20 }}>
            <View
                style={{
                    flex: 1,
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                <View style={{
                    flex: 4,
                    width: '100%',
                    flexDirection: 'column',
                    alignItems: 'center',
                    justifyContent: 'center',
                }}>
                    <View style={{
                        width: 350,
                        height: 500,
                        flexDirection: 'column',
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}>
                        {card.type == CARD_TYPE_RESOURCE && <ResourceCard 
                            initialHidden={initialHidden} 
                            autoReveal={autoReveal} 
                            title={card.name[0]} 
                            color={card.colors[0]} 
                            artwork={getImageAssetById(card.illustrationid)} 
                            cost={card.cost} 
                            effectIcons={[card.effectid]} 
                            effectArgs={card.effectargs} 
                            style={{ flex: 1, width: '90%', alignSelf: 'center' }} 
                            titleStyle={{ fontSize: normalize(16), paddingTop: normalize(16), paddingLeft: normalize(16) * 1.5 }}
                            effectStyle={{ fontSize: normalize(24) }}
                        /> }
                        {card.type == CARD_TYPE_PROJECT && <ProjectCard 
                            initialHidden={initialHidden} 
                            autoReveal={autoReveal} 
                            title={card.name[0]} 
                            colors={card.colors} 
                            payout={card.payout} 
                            artwork={getImageAssetById(card.illustrationid)} 
                            effectIcons={[card.effectid]} 
                            effectArgs={card.effectargs} 
                            style={{ flex: 1, width: '90%', alignSelf: 'center' }} 
                            titleStyle={{ fontSize: normalize(16), paddingTop: normalize(15), paddingLeft: normalize(16) * 1.5 }} 
                            sellStyle={{ fontSize: normalize(32) }}
                        />}
                        {card.type == CARD_TYPE_PERSONA && <PersonaCard 
                            initialHidden={initialHidden} 
                            autoReveal={autoReveal} 
                            title={card.name[0]} 
                            artwork={getPersonaFrontById(card.illustrationid)} 
                            effectIcons={[card.effectid]} 
                            effectArgs={card.effectargs} 
                            style={{ flex: 1, width: '90%', alignSelf: 'center' }} 
                            titleStyle={{ fontSize: normalize(16), paddingTop: normalize(14) }} 
                            effectStyle={{ fontSize: normalize(24) }}
                        />}
                        {card.type == CARD_TYPE_EVENT && <EventCard 
                            initialHidden={initialHidden} 
                            autoReveal={autoReveal} 
                            title={card.name[0]} 
                            artwork={getImageAssetById(card.illustrationid)}
                            effectIcons={[card.effectid]} 
                            effectArgs={card.effectargs} 
                            style={{ flex: 1, width: '90%', alignSelf: 'center' }} 
                            titleStyle={{ fontSize: normalize(16), paddingTop: normalize(15), paddingLeft: normalize(16) * 1.5 }}
                            effectStyle={{ fontSize: normalize(24) }}
                        />}
                    </View>
                </View>
                {(button1Text || button2Text || button3Text || button4Text) &&
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'flex-start', justifyContent: 'center' }}>
                        <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                            {button1Text && <TitleButton title={button1Text} disabled={button1Disabled} onPress={(event: GestureResponderEvent) => onPress(event, route.params?.onButton1Press)} style={{ height: 40 }}/>}
                            {button2Text && <TitleButton title={button2Text} disabled={button1Disabled} onPress={(event: GestureResponderEvent) => onPress(event, route.params?.onButton2Press)} style={{ height: 40, marginTop: 10 }}/>}
                        </View>
                        {(button3Text || button4Text) &&
                            <View style={{ flex: 1, flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
                                {button3Text && <TitleButton title={button3Text} disabled={button1Disabled} onPress={(event: GestureResponderEvent) => onPress(event, route.params?.onButton3Press)} style={{ height: 40 }}/>}
                                {button4Text && <TitleButton title={button4Text} disabled={button1Disabled} onPress={(event: GestureResponderEvent) => onPress(event, route.params?.onButton4Press)} style={{ height: 40, marginTop: 10 }}/>}
                            </View> 
                        }
                </View>
                }
            </View>
            <ViewHeader title={headerTitle} onPressBack={() => navigation.goBack()} />
        </ImageBackground>
    );
}
