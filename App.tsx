/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import React, { useState } from 'react';
import { GestureResponderEvent, ImageSourcePropType, View } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { useFonts } from '@use-expo/font';
import * as Localization from 'expo-localization';
import i18n from 'i18n-js';

import { Log } from './services/log';
import I18nCatalog from './services/i18n';
import { getEnvironment } from './services/globals';

import StartScreen from './screens/start';
import DetailCardScreen from './screens/detailcard';
import Modal from './screens/modal';
import DetailTextScreen from './screens/detailtext';
import SelectCardScreen from './screens/selectcard';
import LobbyScreen from './screens/lobby';
import GameScreen from './screens/game';
import { Card } from './model/card';
import { Player } from './model/player';
import { Project } from './model/project';
import ResumeScreen from './screens/resume';
import SelectProjectScreen from './screens/selectproject';
import { getCardByID } from './services/middleware';

const { authURL } = getEnvironment();

i18n.translations = I18nCatalog();
i18n.locale = Localization.locale;
i18n.fallbacks = true;

export type RootStackParamList = {
  StartScreen: undefined;
  ResumeScreen: {
    gameID: string
    localPlayerID: string
  } | undefined;
  LobbyScreen: {
    inviteCode: string,
    localPlayerName: string,
    localPlayerID: string,
    gameID: string
  } | undefined;
  GameScreen: {
    gameID: string
    localPlayerID: string
    localPlayerName: string
  } | undefined;
  PlayerScreen: {
    player: Player
    projects: Project[]
    onPressProjectCard?: ((cardID: string) => void),
    onPressResourceCard?: ((cardID: string) => void),
  } | undefined;
  SelectProjectScreen: {
    headerTitle: string;
    card: Card
    projects: Project[]
    onSelectProject: ((project: Project) => void),
  } | undefined;
  DetailCardScreen: {
    headerTitle: string;
    card: Card;
    initialHidden: boolean;
    autoReveal: boolean;
    button1Text?: string,
    button1Disabled?: boolean,
    onButton1Press?: ((event: GestureResponderEvent) => void),
    button2Text?: string,
    button2Disabled?: boolean,
    onButton2Press?: ((event: GestureResponderEvent) => void),
    button3Text?: string,
    button3Disabled?: boolean,
    onButton3Press?: ((event: GestureResponderEvent) => void),
    button4Text?: string,
    button4Disabled?: boolean,
    onButton4Press?: ((event: GestureResponderEvent) => void),
  } | undefined;
  SelectCardScreen: {
    headerTitle: string;
    cards: Card[];
    onSelectCard: ((card: Card) => void),
  } | undefined;
  DetailTextScreen: {
    headerTitle: string;
    title: string;
    text: string;
    image?: ImageSourcePropType;
    button1Text?: string,
    button2Text?: string,
    onButton1Press?: ((event: GestureResponderEvent) => void),
    onButton2Press?: ((event: GestureResponderEvent) => void),
  } | undefined;
  Modal: {
    title: string,
    description: string,
    button1Text: string,
    button2Text?: string,
    icon?: ImageSourcePropType,
    onButton1Press: ((event: GestureResponderEvent) => void),
    onButton2Press?: ((event: GestureResponderEvent) => void),
  } | undefined;
};

const Stack = createStackNavigator<RootStackParamList>();

// cardOverlay ensures there is no "while blinking" on screen switch.
function cardOverlay(): JSX.Element {
  return <View style={{ flex: 1, backgroundColor: 'black' }} />;
}

export default function App() {
  const [initialState, setInitialState] = useState<keyof RootStackParamList | undefined>('StartScreen');

  let [fontsLoaded] = useFonts({
    HeaderFont: require('./assets/fonts/NotoSans-Bold.ttf'),
    TextFont: require('./assets/fonts/NotoSans-Regular.ttf'),
  });

  if (!fontsLoaded) {
    return null;
  }

  return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName={initialState}>
          <Stack.Screen
            name="StartScreen"
            options={{
              headerShown: false,
              cardOverlay: () => cardOverlay(),
            }}
          >
            {(props) => <StartScreen {...props} />}
          </Stack.Screen>
          <Stack.Screen
            name="ResumeScreen"
            initialParams={{
              gameID: 'game ID',
              localPlayerID: 'local player ID',
            }}
            options={{
              headerShown: false,
              cardOverlay: () => cardOverlay(),
            }}
          >
            {(props) => <ResumeScreen gameID={''} localPlayerID={''} {...props} />}
          </Stack.Screen>
          <Stack.Screen
            name="LobbyScreen"
            initialParams= {{
              inviteCode: 'lteh6rw',
              localPlayerName: 'Local Player',
            }}
            options={{
              headerShown: false,
              cardOverlay: () => cardOverlay(),
            }}
          >
            {(props) => <LobbyScreen {...props} />}
          </Stack.Screen>
          <Stack.Screen
            name="GameScreen"
            options={{
              headerShown: false,
              cardOverlay: () => cardOverlay(),
            }}
          >
            {(props) => <GameScreen {...props} />}
          </Stack.Screen>
          <Stack.Screen
            name="DetailCardScreen"
            component={DetailCardScreen}
            initialParams={{ 
              headerTitle: 'CARD', 
              card: getCardByID('resource-0'),
              initialHidden: false,
              autoReveal: false,
            }}
            options={{
              headerShown: false,
              cardOverlay: () => cardOverlay(),
            }}
          />
          <Stack.Screen
            name="SelectCardScreen"
            component={SelectCardScreen}
            initialParams={{
              headerTitle: 'CARD',
              cards: [ getCardByID('resource-0')!, getCardByID('resource-1')!, getCardByID('resource-2')! ],
              onSelectCard: (card: Card) => { console.log('card selected: ' + card.ID) },
            }}
            options={{
              headerShown: false,
              cardOverlay: () => cardOverlay(),
            }}
          />
        <Stack.Screen
          name="SelectProjectScreen"
          component={SelectProjectScreen}
          initialParams={{
            headerTitle: 'PROJECT',
            card: getCardByID('resource-0')!,
            onSelectProject: (project: Project) => { console.log('project selected: ' + project.ID) },
          }}
          options={{
            headerShown: false,
            cardOverlay: () => cardOverlay(),
          }}
        />
          <Stack.Screen
            name="DetailTextScreen"
            component={DetailTextScreen}
            initialParams={{ headerTitle: 'COMPENDIUM', text: 'Body Text.', title: 'Title' }}
            options={{
              headerShown: false,
              cardOverlay: () => cardOverlay(),
            }}
          />
          <Stack.Screen
            name="Modal"
            component={Modal}
            initialParams={{ title: 'Are you sure?', description: 'Are you sure you want to execute this action?' }}
            options={{
              presentation: 'transparentModal',
              headerShown: false,
              animationEnabled: true,
              cardStyle: { backgroundColor: 'rgba(0, 0, 0, 0.15)' },
              cardOverlayEnabled: true,
            }}
          />
        </Stack.Navigator>
      </NavigationContainer>
  );
}
