/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import { ImageSourcePropType } from "react-native";

const EffectAssets = [
  {
    id: 'missing',
    src: require('../assets/icons/icon-missing.png'),
  },
  {
    id: 'add_take_pool',
    src: require('../assets/icons/add_take_pool.png'),
  },
  {
    id: 'sell_bonus',
    src: require('../assets/icons/sell_bonus.png'),
  },
  {
    id: 'add_take_resources',
    src: require('../assets/icons/add_take_resources.png'),
  },
  {
    id: 'add_play_card',
    src: require('../assets/icons/add_play_card.png'),
  },
  {
    id: 'sell_bonus_color',
    src: require('../assets/icons/sell_bonus_color.png'),
  },
  {
    id: 'credit_bonus',
    src: require('../assets/icons/credit_bonus.png'),
  },
  {
    id: 'add_draw_discard_pool',
    src: require('../assets/icons/add_draw_discard_pool.png'),
  },
  {
    id: 'add_getpool_discard_pool',
    src: require('../assets/icons/add_getpool_discard_pool.png'),
  },
  {
    id: 'projectwithcards',
    src: require('../assets/icons/projectwithcards.png'),
  },
  {
    id: 'topool',
    src: require('../assets/icons/topool.png'),
  },
  {
    id: 'toproject',
    src: require('../assets/icons/toproject.png'),
  },
];

const ResourceFrontAssets = [
  {
    id: 'missing',
    src: require('../assets/ui/icon-missing.png'),
  },
  {
    id: 'a',
    src: require('../assets/cards/front-resource-blue.png'),
  },
  {
    id: 'b',
    src: require('../assets/cards/front-resource-green.png'),
  },
  {
    id: 'c',
    src: require('../assets/cards/front-resource-purple.png'),
  },
  {
    id: 'd',
    src: require('../assets/cards/front-resource-red.png'),
  },
  {
    id: 'e',
    src: require('../assets/cards/front-resource-rose.png'),
  },
  {
    id: 'f',
    src: require('../assets/cards/front-resource-yellow.png'),
  },
];

const PersonaFrontAssets = [
  {
    id: 'missing',
    src: require('../assets/ui/icon-missing.png'),
  },
  {
    id: 'blogger',
    src: require('../assets/cards/front-persona-blogger.png'),
  },
  {
    id: 'gamer',
    src: require('../assets/cards/front-persona-gamer.png'),
  },
  {
    id: 'musician',
    src: require('../assets/cards/front-persona-musician.png'),
  },
  {
    id: 'podcaster',
    src: require('../assets/cards/front-persona-podcaster.png'),
  },
  {
    id: 'writer',
    src: require('../assets/cards/front-persona-writer.png'),
  },
  {
    id: 'filmmaker',
    src: require('../assets/cards/front-persona-filmmaker.png'),
  },
];

const ColorAssets = [
  {
    id: 'missing',
    src: require('../assets/ui/icon-missing.png'),
  },
  {
    id: 'a',
    src: require('../assets/colorblocks/colorblock-blue.png'),
  },
  {
    id: 'b',
    src: require('../assets/colorblocks/colorblock-green.png'),
  },
  {
    id: 'c',
    src: require('../assets/colorblocks/colorblock-purple.png'),
  },
  {
    id: 'd',
    src: require('../assets/colorblocks/colorblock-red.png'),
  },
  {
    id: 'e',
    src: require('../assets/colorblocks/colorblock-rose.png'),
  },
  {
    id: 'f',
    src: require('../assets/colorblocks/colorblock-yellow.png'),
  },
];

// TODO: all dynamically loaded images need to be added here.
const ImageAssets = [
  {
    id: 'missing',
    src: require('../assets/ui/icon-missing.png'),
  },
  // ARTWORKS
  { id: 'audiomixer', src: require('../assets/artworks/audiomixer.png'), },
  { id: 'audiorecorder', src: require('../assets/artworks/audiorecorder.png'), },
  { id: 'audiosoftware2', src: require('../assets/artworks/audiosoftware2.png'), },
  { id: 'audiosoftware3', src: require('../assets/artworks/audiosoftware3.png'), },
  { id: 'audiosoftware', src: require('../assets/artworks/audiosoftware.png'), },
  { id: 'author', src: require('../assets/artworks/author.png'), },
  { id: 'backgroundsinger', src: require('../assets/artworks/backgroundsinger.png'), },
  { id: 'blogger', src: require('../assets/artworks/blogger.png'), },
  { id: 'bloggertrainee', src: require('../assets/artworks/bloggertrainee.png'), },
  { id: 'blogsoftware', src: require('../assets/artworks/blogsoftware.png'), },
  { id: 'coffee3', src: require('../assets/artworks/coffee3.png'), },
  { id: 'coffee4', src: require('../assets/artworks/coffee4.png'), },
  { id: 'coffee5', src: require('../assets/artworks/coffee5.png'), },
  { id: 'coffee6', src: require('../assets/artworks/coffee6.png'), },
  { id: 'coffee', src: require('../assets/artworks/coffee.png'), },
  { id: 'communitymanager', src: require('../assets/artworks/communitymanager.png'), },
  { id: 'developertraining', src: require('../assets/artworks/developertraining.png'), },
  { id: 'drummer', src: require('../assets/artworks/drummer.png'), },
  { id: 'drums', src: require('../assets/artworks/drums.png'), },
  { id: 'editor', src: require('../assets/artworks/editor.png'), },
  { id: 'gameartist', src: require('../assets/artworks/gameartist.png'), },
  { id: 'gameengine', src: require('../assets/artworks/gameengine.png'), },
  { id: 'gamemusiccomposer', src: require('../assets/artworks/gamemusiccomposer.png'), },
  { id: 'greenscreen', src: require('../assets/artworks/greenscreen.png'), },
  { id: 'guestbooker', src: require('../assets/artworks/guestbooker.png'), },
  { id: 'guitar', src: require('../assets/artworks/guitar.png'), },
  { id: 'headphones2', src: require('../assets/artworks/headphones2.png'), },
  { id: 'headphones', src: require('../assets/artworks/headphones.png'), },
  { id: 'highendcamera', src: require('../assets/artworks/highendcamera.png'), },
  { id: 'highendheadphones', src: require('../assets/artworks/highendheadphones.png'), },
  { id: 'highendmicrophone', src: require('../assets/artworks/highendmicrophone.png'), },
  { id: 'ideaprompts', src: require('../assets/artworks/ideaprompts.png'), },
  { id: 'instrumentmicrophone', src: require('../assets/artworks/instrumentmicrophone.png'), },
  { id: 'introspeaker', src: require('../assets/artworks/introspeaker.png'), },
  { id: 'juniorgamedeveloper2', src: require('../assets/artworks/juniorgamedeveloper2.png'), },
  { id: 'juniorgamedeveloper', src: require('../assets/artworks/juniorgamedeveloper.png'), },
  { id: 'juniorvideohost', src: require('../assets/artworks/juniorvideohost.png'), },
  { id: 'keyboarder', src: require('../assets/artworks/keyboarder.png'), },
  { id: 'keyboard', src: require('../assets/artworks/keyboard.png'), },
  { id: 'lavaliermicrophone', src: require('../assets/artworks/lavaliermicrophone.png'), },
  { id: 'lightgels', src: require('../assets/artworks/lightgels.png'), },
  { id: 'livestreamingcontroller', src: require('../assets/artworks/livestreamingcontroller.png'), },
  { id: 'lowendcamera', src: require('../assets/artworks/lowendcamera.png'), },
  { id: 'microphone', src: require('../assets/artworks/microphone.png'), },
  { id: 'midendcamera', src: require('../assets/artworks/midendcamera.png'), },
  { id: 'minimicrophone', src: require('../assets/artworks/minimicrophone.png'), },
  { id: 'notebook', src: require('../assets/artworks/notebook.png'), },
  { id: 'podcasteditor', src: require('../assets/artworks/podcasteditor.png'), },
  { id: 'podcasthost', src: require('../assets/artworks/podcasthost.png'), },
  { id: 'professionalkeyboard', src: require('../assets/artworks/professionalkeyboard.png'), },
  { id: 'professionalstudiolight', src: require('../assets/artworks/professionalstudiolight.png'), },
  { id: 'publicist', src: require('../assets/artworks/publicist.png'), },
  { id: 'ringlight', src: require('../assets/artworks/ringlight.png'), },
  { id: 'seniorcommunitymanager', src: require('../assets/artworks/seniorcommunitymanager.png'), },
  { id: 'senioreditor', src: require('../assets/artworks/senioreditor.png'), },
  { id: 'seniorgamedeveloper2', src: require('../assets/artworks/seniorgamedeveloper2.png'), },
  { id: 'seniorgamedeveloper', src: require('../assets/artworks/seniorgamedeveloper.png'), },
  { id: 'singer', src: require('../assets/artworks/singer.png'), },
  { id: 'singingmicrophone', src: require('../assets/artworks/singingmicrophone.png'), },
  { id: 'socialmediaagency', src: require('../assets/artworks/socialmediaagency.png'), },
  { id: 'studiolight', src: require('../assets/artworks/studiolight.png'), },
  { id: 'studiotechnician', src: require('../assets/artworks/studiotechnician.png'), },
  { id: 'videoeditor', src: require('../assets/artworks/videoeditor.png'), },
  { id: 'videofilter', src: require('../assets/artworks/videofilter.png'), },
  { id: 'videohost', src: require('../assets/artworks/videohost.png'), },
  { id: 'videosoftware', src: require('../assets/artworks/videosoftware.png'), },
  { id: 'voicetrainer', src: require('../assets/artworks/voicetrainer.png'), },
  { id: 'webserver', src: require('../assets/artworks/webserver.png'), },
  { id: 'writtingsoftware', src: require('../assets/artworks/writtingsoftware.png'), },
  { id: 'youngauthor', src: require('../assets/artworks/youngauthor.png'), },
];

export const getImageAssetById = (id: string): ImageSourcePropType => {
  const imageAsset = ImageAssets.find(imageAsset => imageAsset.id === id);
  if (imageAsset) {
    return imageAsset.src;
  }
  return ImageAssets[0].src;
};

export const getEffectIconById = (id: string): ImageSourcePropType => {
  const imageAsset = EffectAssets.find(imageAsset => imageAsset.id === id);
  if (imageAsset) {
    return imageAsset.src;
  }
  return EffectAssets[0].src;
};

export const getColorBlockByColorId = (id: string): ImageSourcePropType => {
  const imageAsset = ColorAssets.find(imageAsset => imageAsset.id === id);
  if (imageAsset) {
    return imageAsset.src;
  }
  return ColorAssets[0].src;
};

export const getResourceFrontByColorId = (id: string): ImageSourcePropType => {
  const imageAsset = ResourceFrontAssets.find(imageAsset => imageAsset.id === id);
  if (imageAsset) {
    return imageAsset.src;
  }
  return ResourceFrontAssets[0].src;
};

export const getPersonaFrontById = (id: string): ImageSourcePropType => {
  const imageAsset = PersonaFrontAssets.find(imageAsset => imageAsset.id === id);
  if (imageAsset) {
    return imageAsset.src;
  }
  return PersonaFrontAssets[0].src;
};
