/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import { Dimensions, PixelRatio, Platform } from 'react-native';
import Constants from 'expo-constants';
import * as Updates from 'expo-updates';

// persistent settings keys
export const SETTINGS_PLAYERID = 'authinfo_playerid'
export const SETTINGS_SESSIONTOKEN = 'authinfo_sessiontoken'

// app context
export const CONTEXT = 'launchpad-' + Updates.releaseChannel ? Updates.releaseChannel : 'dev';

// game states
export const GAME_STATE_NEW: string = 'new'
export const GAME_STATE_RUNNING: string = 'running'
export const GAME_STATE_ENDED: string = 'ended'

// card types
export const CARD_TYPE_RESOURCE = 'resource';
export const CARD_TYPE_PROJECT = 'project';
export const CARD_TYPE_PERSONA = 'persona';
export const CARD_TYPE_EVENT = 'event';

// card colors
export const CARD_COLOR_RED = 'red';
export const CARD_COLOR_ORANGE = 'orange';
export const CARD_COLOR_YELLOW = 'yellow';
export const CARD_COLOR_GREEN = 'green';
export const CARD_COLOR_BLUE = 'blue';
export const CARD_COLOR_PURPLE = 'purple';
export const CARD_COLOR_CYAN = 'cyan';

// based on OnePlus 6 scale (orig 400)
const { width: SCREEN_WIDTH, height: SCREEN_HEIGHT } = Dimensions.get('window');
const scale = SCREEN_WIDTH / 370;

export const normalize = (size: number) => {
    const newSize = size * scale;
    if (Platform.OS === 'ios') {
        return Math.round(PixelRatio.roundToNearestPixel(newSize));
    } else {
        return Math.round(PixelRatio.roundToNearestPixel(newSize)) - 2;
    }
};

export const getEnvironment: any = (env = Updates.releaseChannel) => {
    // __DEV__ is true when run locally, but false when published.
    if (__DEV__) {
        return {
            serviceURL: Constants.manifest!.extra!.serviceURLDev
        };
    } else if (env === 'staging') {
        return {
            serviceURL: Constants.manifest!.extra!.serviceURLStaging
        };
    } else 
        return {
            serviceURL: Constants.manifest!.extra!.serviceURLProduction
        };
};