/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

class CircularStringArray extends Array {
    maxLength: number;

    constructor(maxLength: number) {
        super();
        this.maxLength = maxLength;
    }

    push(elements: any[]): number {
        let result = super.push(elements);
        while (this.length > this.maxLength) {
            this.shift();
        }
        return result;
    }
}

export class Log {
    static buffer = new CircularStringArray(100);
    static startTimestamp = new Date().getTime();

    static info(message: string) {
        let timestamp = new Date().getTime();
        let elems: string[] = ['INFO ' + (timestamp - Log.startTimestamp) / 1000 + ' - ' + message];
        Log.buffer.push(elems);
        console.log(message);
    }

    static warn(message: string) {
        let timestamp = new Date().getTime();
        let elems: string[] = ['WARN ' + (timestamp - Log.startTimestamp) / 1000 + ' - ' + message];
        Log.buffer.push(elems);
        console.warn(message);
    }

    static error(message: string) {
        let timestamp = new Date().getTime();
        let elems: string[] = ['ERROR ' + (timestamp - Log.startTimestamp) / 1000 + ' - ' + message];
        Log.buffer.push(elems);
        console.error(message);
    }
}
