/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import AsyncStorage from '@react-native-async-storage/async-storage';
import * as Device from 'expo-device';
import { Log } from './log';

export const configKey = '@launchpad_config';

export class Settings {
    // sets config key-value, returns error if error.
    static async setConfig(key: string, value: any) {
        let config = await this.getConfigObj();
        if (!config) {
            Log.warn('error retrieving config for writing');
            return 'error retrieving config for writing';
        }
        config[key] = value;
        try {
            await AsyncStorage.setItem(configKey, JSON.stringify(config));
        } catch (e: any) {
            Log.warn(e);
            return e;
        }
    }

    // returns config value for key or undefined on error or not existing.
    static async getConfig(key: string) {
        try {
            let configObj = await this.getConfigObj();
            if (!configObj) {
                return undefined;
            }
            return configObj[key];
        } catch (e: any) {
            Log.warn(e);
            return undefined;
        }
    }

    // returns config or {} if empty config; returns undefined on error.
    static async getConfigObj() {
        try {
            let configJsonValue = await AsyncStorage.getItem(configKey);
            if (!configJsonValue) {
                Log.info('no existing config found.');
                return {};
            }
            let configObj = JSON.parse(configJsonValue);
            return configObj;
        } catch (e: any) {
            Log.warn(e);
            return undefined;
        }
    }

    // wipes the config.
    static async wipeConfig() {
        try {
            await AsyncStorage.removeItem(configKey);
        } catch (e: any) {
            Log.warn(e);
        }
    }

    // wipes all async storage; this will factory-reset the app.
    static async wipeAll() {
        Log.info('wiping all storage..');
        try {
            await AsyncStorage.clear();
        } catch (e: any) {
            Log.warn(e);
        }
    }
}
