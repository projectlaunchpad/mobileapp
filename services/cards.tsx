/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */
/*
import { Log } from "./log";
import { Card } from "../model/card";
import { getAllCards } from "./middleware";

var cards: Card[] = [];

export const getCardByID = (id: string): Card | undefined => cards.find((card) => card.ID === id);

export const fetchCards = async () => {
    Log.info("fetching cards...");
    await getAllCards().then((fetchedCards) => {
        cards = fetchedCards;
        Log.info("fetched " + fetchedCards.length + " cards");
    }).catch((error) => {
        Log.error(error);
    });
}
*/
    /*
    {
        "ID": "resource-0",
        "type": "resource",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 0", "Name 0"],
        "description": ["Description 0", "Beschreibung 0"],
        "effectid": "playEffect0",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "resource-1",
        "type": "resource",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "resource-2",
        "type": "resource",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 0", "Name 0"],
        "description": ["Description 0", "Beschreibung 0"],
        "effectid": "playEffect0",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "resource-3",
        "type": "resource",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "resource-4",
        "type": "resource",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 0", "Name 0"],
        "description": ["Description 0", "Beschreibung 0"],
        "effectid": "playEffect0",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "resource-5",
        "type": "resource",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "resource-6",
        "type": "resource",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 0", "Name 0"],
        "description": ["Description 0", "Beschreibung 0"],
        "effectid": "playEffect0",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "resource-7",
        "type": "resource",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "resource-8",
        "type": "resource",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 0", "Name 0"],
        "description": ["Description 0", "Beschreibung 0"],
        "effectid": "playEffect0",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "resource-9",
        "type": "resource",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "resource-10",
        "type": "resource",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 0", "Name 0"],
        "description": ["Description 0", "Beschreibung 0"],
        "effectid": "playEffect0",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "resource-11",
        "type": "resource",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "resource-12",
        "type": "resource",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },


    {
        "ID": "project-0",
        "type": "project",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "project-1",
        "type": "project",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "project-2",
        "type": "project",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "project-3",
        "type": "project",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "project-4",
        "type": "project",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "project-5",
        "type": "project",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "project-6",
        "type": "project",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "project-7",
        "type": "project",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },


    {
        "ID": "persona-0",
        "type": "persona",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "persona-1",
        "type": "persona",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "persona-2",
        "type": "persona",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "persona-3",
        "type": "persona",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "persona-4",
        "type": "persona",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },



    {
        "ID": "event-0",
        "type": "event",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "event-1",
        "type": "event",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    },
    {
        "ID": "event-2",
        "type": "event",
        "illustrationid": "resource-0",
        "iconid": "resource-0",
        "name": ["Name 1", "Name 1"],
        "description": ["Description 1", "Beschreibung 1"],
        "effectid": "playEffect1",
        "effectargs": ["blue", "-1"],
        "colors": ["red"],
        "cost": 1,
        "payout": {
            "1": 1,
            "2": 2,
            "3": 3
        }
    }
*/