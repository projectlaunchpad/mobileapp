/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import axios from 'axios';

import { getEnvironment, SETTINGS_PLAYERID, SETTINGS_SESSIONTOKEN } from './globals';
import { Log } from './log';
import { Player } from '../model/player';
import { Card } from '../model/card';
import { Game } from '../model/game';
import { Settings } from './settings';
import { Stack } from '../model/stack';
import { Action } from '../model/action';
import { GameResult } from '../model/gameresult';

const ServiceURL: string = getEnvironment().serviceURL;

export var fetchCardsURL = () => `${ServiceURL}/data/cards`;
export var createGameURL = (playername: string) => `${ServiceURL}/game/create/${playername}`;
export var joinGameURL = (invitecode: string, playername: string) => `${ServiceURL}/game/join/${invitecode}/${playername}`;
export var getGameURL = (gameid: string) => `${ServiceURL}/game/state/${gameid}`;
export var startGameURL = (gameid: string) => `${ServiceURL}/game/start/${gameid}`;

export var getStackURL = (gameid: string, stackid: string) => `${ServiceURL}/game/data/stack/${gameid}/${stackid}`;
export var getHandURL = (gameid: string, handid: string) => `${ServiceURL}/game/data/hand/${gameid}/${handid}`;
export var getPlayerURL = (gameid: string, playerid: string) => `${ServiceURL}/game/data/player/${gameid}/${playerid}`;
export var getProjectURL = (gameid: string, projectid: string) => `${ServiceURL}/game/data/project/${gameid}/${projectid}`;
export var getActionURL = (gameid: string, actionid: string) => `${ServiceURL}/game/data/action/${gameid}/${actionid}`;
export var drawResourceURL = (gameid: string) => `${ServiceURL}/action/${gameid}/resource/draw`;
export var drawProjectsURL = (gameid: string) => `${ServiceURL}/action/${gameid}/project/draw`;
export var acceptProjectURL = (gameid: string, projectcardid: string) => `${ServiceURL}/action/${gameid}/project/accept/${projectcardid}`;
export var acquireFromPoolURL = (gameid: string) => `${ServiceURL}/action/${gameid}/pool/acquire`;
export var playToPoolURL = (gameid: string, resourcecardid: string) => `${ServiceURL}/action/${gameid}/pool/${resourcecardid}`;
export var playResourceToProjectURL = (gameid: string, projectid: string, resourcecardid: string) => `${ServiceURL}/action/${gameid}/add/${projectid}/${resourcecardid}`;
export var sellProjectURL = (gameid: string, projectid: string) => `${ServiceURL}/action/${gameid}/sell/${projectid}`;
export var endTurnURL = (gameid: string) => `${ServiceURL}/action/${gameid}/turn`;
export var giveCardURL = (gameid: string, resourcecardid: string, playerid: string) => `${ServiceURL}/action/${gameid}/givecard/${resourcecardid}/${playerid}`;
export var giveCreditsURL = (gameid: string, creditsvalue: number, playerid: string) => `${ServiceURL}/action/${gameid}/givecredits/${creditsvalue}/${playerid}`;
export var endGameURL = (gameid: string) => `${ServiceURL}/action/${gameid}/end`;

var cards: Card[] = [];

export function getCardByID(id: string): Card | undefined {
    return cards.find((card) => card.ID === id);
}

export async function fetchCards(): Promise<void> {
    Log.info("fetching cards...");
    await getAllCards().then((fetchedCards) => {
        cards = fetchedCards;
        Log.info("fetched " + fetchedCards.length + " cards");
    }).catch((error) => {
        Log.error(error);
    });
}

export async function getAllCards(): Promise<Card[] | any> {
    try {
        const response = await axios.get(fetchCardsURL());
        Log.info('fetched card list from server, got ' + response.data.length + ' cards');
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('fetching cards from server failed: ' + error);
        return await Promise.reject(error);
    }
}

export async function createGame(playername: string): Promise<Player | any> {
    try {
        const response = await axios.get(createGameURL(playername));
        Log.info('created game for player name ' + playername + ' game id ' + response.data.gameid);
        await Settings.setConfig(SETTINGS_PLAYERID, response.data.ID);
        await Settings.setConfig(SETTINGS_SESSIONTOKEN, response.data.sessionToken);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('create game failed for player name ' + playername + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function joinGame(inviteCode: string, playername: string): Promise<Player | any> {
    try {
        const response = await axios.get(joinGameURL(inviteCode, playername));
        Log.info('joined game with player name ' + playername + ' game id ' + response.data.gameid);
        await Settings.setConfig(SETTINGS_PLAYERID, response.data.ID);
        await Settings.setConfig(SETTINGS_SESSIONTOKEN, response.data.sessionToken);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('join game failed for player name ' + playername + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function getGame(gameid: string): Promise<Game | any> {
    let playerID = await Settings.getConfig(SETTINGS_PLAYERID);
    let sessionToken = await Settings.getConfig(SETTINGS_SESSIONTOKEN);
    try {
        let config = {
            headers: {
                'Launchpad-Player': playerID,
                'Launchpad-Token': sessionToken
            }
        }
        const response = await axios.get(getGameURL(gameid), config);
        Log.info('retrieved game id ' + response.data.ID);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('retrieving game failed for game id ' + gameid + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function startGame(gameid: string): Promise<Game | any> {
    let playerID = await Settings.getConfig(SETTINGS_PLAYERID);
    let sessionToken = await Settings.getConfig(SETTINGS_SESSIONTOKEN);
    try {
        let config = {
            headers: {
                'Launchpad-Player': playerID,
                'Launchpad-Token': sessionToken
            }
        }
        const response = await axios.get(startGameURL(gameid), config);
        Log.info('started game id ' + response.data.gameid);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('starting game failed for game id ' + gameid + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function getPlayer(gameid: string, queryPlayerID: string): Promise<Player | any> {
    let playerID = await Settings.getConfig(SETTINGS_PLAYERID);
    let sessionToken = await Settings.getConfig(SETTINGS_SESSIONTOKEN);
    try {
        let config = {
            headers: {
                'Launchpad-Player': playerID,
                'Launchpad-Token': sessionToken
            }
        }
        const response = await axios.get(getPlayerURL(gameid, queryPlayerID), config);
        Log.info('retrieved player id ' + response.data.ID);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('retrieving player failed for player id ' + queryPlayerID + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function getStack(gameid: string, stackID: string): Promise<Stack | any> {
    let playerID = await Settings.getConfig(SETTINGS_PLAYERID);
    let sessionToken = await Settings.getConfig(SETTINGS_SESSIONTOKEN);
    try {
        let config = {
            headers: {
                'Launchpad-Player': playerID,
                'Launchpad-Token': sessionToken
            }
        }
        const response = await axios.get(getStackURL(gameid, stackID), config);
        Log.info('retrieved stack id ' + response.data.ID);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('retrieving stack failed for id ' + stackID + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function getHand(gameid: string, handID: string): Promise<Stack | any> {
    let playerID = await Settings.getConfig(SETTINGS_PLAYERID);
    let sessionToken = await Settings.getConfig(SETTINGS_SESSIONTOKEN);
    try {
        let config = {
            headers: {
                'Launchpad-Player': playerID,
                'Launchpad-Token': sessionToken
            }
        }
        const response = await axios.get(getHandURL(gameid, handID), config);
        Log.info('retrieved hand id ' + response.data.ID);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('retrieving hand failed for id ' + handID + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function getProject(gameid: string, projectID: string): Promise<Stack | any> {
    let playerID = await Settings.getConfig(SETTINGS_PLAYERID);
    let sessionToken = await Settings.getConfig(SETTINGS_SESSIONTOKEN);
    try {
        let config = {
            headers: {
                'Launchpad-Player': playerID,
                'Launchpad-Token': sessionToken
            }
        }
        const response = await axios.get(getProjectURL(gameid, projectID), config);
        Log.info('retrieved project id ' + response.data.ID);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('retrieving project failed for id ' + projectID + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function getAction(gameid: string, actionID: string): Promise<Action | any> {
    let playerID = await Settings.getConfig(SETTINGS_PLAYERID);
    let sessionToken = await Settings.getConfig(SETTINGS_SESSIONTOKEN);
    try {
        let config = {
            headers: {
                'Launchpad-Player': playerID,
                'Launchpad-Token': sessionToken
            }
        }
        const response = await axios.get(getActionURL(gameid, actionID), config);
        Log.info('retrieved action id ' + response.data.ID);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('retrieving action failed for id ' + actionID + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function drawResourceFromDeck(gameid: string): Promise<Card | any> {
    let playerID = await Settings.getConfig(SETTINGS_PLAYERID);
    let sessionToken = await Settings.getConfig(SETTINGS_SESSIONTOKEN);
    try {
        let config = {
            headers: {
                'Launchpad-Player': playerID,
                'Launchpad-Token': sessionToken
            }
        }
        const response = await axios.get(drawResourceURL(gameid), config);
        Log.info('drawed resource card id ' + response.data.ID);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('drawing resource card failed for game id ' + gameid + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function drawProjectCards(gameid: string): Promise<Card[] | any> {
    let playerID = await Settings.getConfig(SETTINGS_PLAYERID);
    let sessionToken = await Settings.getConfig(SETTINGS_SESSIONTOKEN);
    try {
        let config = {
            headers: {
                'Launchpad-Player': playerID,
                'Launchpad-Token': sessionToken
            }
        }
        const response = await axios.get(drawProjectsURL(gameid), config);
        let cards: Card[] = [];
        Log.info('drawed project cards or game id ' + gameid);
        if (response.data && response.data.length > 0) {
            for (let cardID of response.data) {
                let thisCard = getCardByID(cardID);
                if (thisCard) {
                    cards.push(thisCard);
                }
            }
        }   
        return await Promise.resolve(cards);
    } catch (error) {
        Log.error('drawing project cards failed for game id ' + gameid + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function getResourceFromPool(gameid: string): Promise<Card | any> {
    let playerID = await Settings.getConfig(SETTINGS_PLAYERID);
    let sessionToken = await Settings.getConfig(SETTINGS_SESSIONTOKEN);
    try {
        let config = {
            headers: {
                'Launchpad-Player': playerID,
                'Launchpad-Token': sessionToken
            }
        }
        const response = await axios.get(acquireFromPoolURL(gameid), config);
        Log.info('got card from pool: ' + response.data.ID);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('getting card from pool failed for game id ' + gameid + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function acceptProjectCard(gameid: string, projectcardid: string): Promise<Player | any> {
    let playerID = await Settings.getConfig(SETTINGS_PLAYERID);
    let sessionToken = await Settings.getConfig(SETTINGS_SESSIONTOKEN);
    try {
        let config = {
            headers: {
                'Launchpad-Player': playerID,
                'Launchpad-Token': sessionToken
            }
        }
        const response = await axios.put(acceptProjectURL(gameid, projectcardid), {}, config);
        Log.info('accepted project card: ' + response.data.ID);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('accepting project card failed for project card id ' + projectcardid + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function discardResourceToPool(gameid: string, resourcecardid: string): Promise<Game | any> {
    let playerID = await Settings.getConfig(SETTINGS_PLAYERID);
    let sessionToken = await Settings.getConfig(SETTINGS_SESSIONTOKEN);
    try {
        let config = {
            headers: {
                'Launchpad-Player': playerID,
                'Launchpad-Token': sessionToken
            }
        }
        const response = await axios.put(playToPoolURL(gameid, resourcecardid), {}, config);
        Log.info('discarded card to pool: ' + response.data.ID);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('discarding to pool failed for card id ' + resourcecardid + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function playResourceToProject(gameid: string, projectid: string, resourcecardid: string): Promise<Player | any> {
    let playerID = await Settings.getConfig(SETTINGS_PLAYERID);
    let sessionToken = await Settings.getConfig(SETTINGS_SESSIONTOKEN);
    try {
        let config = {
            headers: {
                'Launchpad-Player': playerID,
                'Launchpad-Token': sessionToken
            }
        }
        const response = await axios.put(playResourceToProjectURL(gameid, projectid, resourcecardid), {}, config);
        Log.info('added card to project for player ' + response.data.ID);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('error playing card to project card id ' + resourcecardid + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function sellProject(gameid: string, projectid: string): Promise<Player | any> {
    let playerID = await Settings.getConfig(SETTINGS_PLAYERID);
    let sessionToken = await Settings.getConfig(SETTINGS_SESSIONTOKEN);
    try {
        let config = {
            headers: {
                'Launchpad-Player': playerID,
                'Launchpad-Token': sessionToken
            }
        }
        const response = await axios.put(sellProjectURL(gameid, projectid), {}, config);
        Log.info('old project for player ' + response.data.ID);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('error selling project ' + projectid + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function endTurn(gameid: string): Promise<Game | any> {
    let playerID = await Settings.getConfig(SETTINGS_PLAYERID);
    let sessionToken = await Settings.getConfig(SETTINGS_SESSIONTOKEN);
    try {
        let config = {
            headers: {
                'Launchpad-Player': playerID,
                'Launchpad-Token': sessionToken
            }
        }
        const response = await axios.put(endTurnURL(gameid), {}, config);
        Log.info('ended turn for game ' + response.data.ID);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('error ending turn for game ' + gameid + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function endGame(gameid: string): Promise<GameResult | any> {
    let playerID = await Settings.getConfig(SETTINGS_PLAYERID);
    let sessionToken = await Settings.getConfig(SETTINGS_SESSIONTOKEN);
    try {
        let config = {
            headers: {
                'Launchpad-Player': playerID,
                'Launchpad-Token': sessionToken
            }
        }
        const response = await axios.put(endGameURL(gameid), {}, config);
        Log.info('ended game ' + gameid + ', winner is ' + response.data.winnerplayerid);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('error ending game ' + gameid + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function giveCardToPlayer(gameid: string, resourcecardid: string, playerid: string): Promise<Player | any> {
    let playerID = await Settings.getConfig(SETTINGS_PLAYERID);
    let sessionToken = await Settings.getConfig(SETTINGS_SESSIONTOKEN);
    try {
        let config = {
            headers: {
                'Launchpad-Player': playerID,
                'Launchpad-Token': sessionToken
            }
        }
        const response = await axios.put(giveCardURL(gameid, resourcecardid, playerid), {}, config);
        Log.info('player has given card: ' + response.data.ID);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('error giving card from player ' + playerid + ': ' + error);
        return await Promise.reject(error);
    }
}

export async function giveCreditsToPlayer(gameid: string, creditsvalue: number, playerid: string): Promise<Player | any> {
    let playerID = await Settings.getConfig(SETTINGS_PLAYERID);
    let sessionToken = await Settings.getConfig(SETTINGS_SESSIONTOKEN);
    try {
        let config = {
            headers: {
                'Launchpad-Player': playerID,
                'Launchpad-Token': sessionToken
            }
        }
        const response = await axios.put(giveCreditsURL(gameid, creditsvalue, playerid), {}, config);
        Log.info('player has given credits: ' + response.data.ID);
        return await Promise.resolve(response.data);
    } catch (error) {
        Log.error('error giving credits from player ' + playerid + ': ' + error);
        return await Promise.reject(error);
    }
}
