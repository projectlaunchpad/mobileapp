/*
 * Launchpad Frontend
 * Copyright 2021 The Launchpad Team
 * Licensed under the terms of the MIT License, see LICENSE file for detail.
 */

import i18n from 'i18n-js';

export const isDELocale = function () {
    let l = i18n.currentLocale();
    if (l && l.toLowerCase().startsWith('de')) return true;
    else return false;
};

export default function I18nCatalog() {
    return {
        en: {
            yes: 'Yes',
            no: 'No',
            ok: 'Ok',
            cancel: 'Cancel',
            back: 'Back',
            loginScreenTitle: 'LOGIN',
            heroScreenTitle: 'HERO',
            battleScreenTitle: 'BATTLE',
        },
        de: {
            yes: 'Ja',
            no: 'Nein',
            ok: 'Ok',
            cancel: 'Abbrechen',
            back: 'Zurück',
            loginScreenTitle: 'ANMELDEN',
            heroScreenTitle: 'HELD',
            battleScreenTitle: 'KAMPF',
        },
    };
}
